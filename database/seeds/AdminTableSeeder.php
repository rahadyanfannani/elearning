<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('admin')->delete();

        \DB::table('admin')->insert(array (
            0 =>
                array (
                    'nama' => 'Admin',
                    'email' => 'admin@elearning.com',
                    'password' => bcrypt('secret'),
                    'created_at' => '2017-04-17 22:06:09',
                    'updated_at' => '2017-04-17 22:06:07',
                )
        ));

    }
}
