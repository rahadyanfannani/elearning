<?php

use Illuminate\Database\Seeder;

class KelasFisikTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kelas_fisik')->delete();
        
        \DB::table('kelas_fisik')->insert(array (
            0 => 
            array (
                'id_kelas' => 11014011,
                'id_prodi' => 12014011,
                'nama_kelas' => 'S1 PTI A 2014',
                'created_at' => '2017-04-12 23:34:20',
                'updated_at' => '2017-04-12 23:34:22',
            ),
            1 => 
            array (
                'id_kelas' => 11014012,
                'id_prodi' => 12014012,
                'nama_kelas' => 'S1 PTI B 2014',
                'created_at' => '2017-04-12 23:35:14',
                'updated_at' => '2017-04-12 23:35:17',
            ),
            2 => 
            array (
                'id_kelas' => 11014013,
                'id_prodi' => 12014013,
                'nama_kelas' => 'S1 PTI C 2014',
                'created_at' => '2017-04-12 23:36:12',
                'updated_at' => '2017-04-12 23:36:14',
            ),
            3 => 
            array (
                'id_kelas' => 11014014,
                'id_prodi' => 12014014,
                'nama_kelas' => 'S1 PTI D 2014',
                'created_at' => '2017-04-12 23:37:25',
                'updated_at' => '2017-04-12 23:37:27',
            ),
            4 => 
            array (
                'id_kelas' => 11014015,
                'id_prodi' => 12014015,
                'nama_kelas' => 'S1 PTI E 2014',
                'created_at' => '2017-04-12 23:38:03',
                'updated_at' => '2017-04-12 23:38:05',
            ),
            5 => 
            array (
                'id_kelas' => 11014016,
                'id_prodi' => 12014016,
                'nama_kelas' => 'S1 PTE A 2014',
                'created_at' => '2017-04-13 07:28:48',
                'updated_at' => '2017-04-13 07:28:50',
            ),
            6 => 
            array (
                'id_kelas' => 11014017,
                'id_prodi' => 12014017,
                'nama_kelas' => 'S1 PTE B 2014',
                'created_at' => '2017-04-13 07:29:23',
                'updated_at' => '2017-04-13 07:29:26',
            ),
            7 => 
            array (
                'id_kelas' => 11014018,
                'id_prodi' => 12014018,
                'nama_kelas' => 'S1 PTE C 2014',
                'created_at' => '2017-04-13 07:29:56',
                'updated_at' => '2017-04-13 07:29:59',
            ),
            8 => 
            array (
                'id_kelas' => 11014019,
                'id_prodi' => 12014019,
                'nama_kelas' => 'S1 PTE D 2014',
                'created_at' => '2017-04-13 07:30:42',
                'updated_at' => '2017-04-13 07:30:44',
            ),
            9 => 
            array (
                'id_kelas' => 11014020,
                'id_prodi' => 12014020,
                'nama_kelas' => 'S1 PTE E 2014',
                'created_at' => '2017-04-13 07:31:21',
                'updated_at' => '2017-04-13 07:31:24',
            ),
        ));
        
        
    }
}