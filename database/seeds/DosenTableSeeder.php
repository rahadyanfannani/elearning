<?php

use Illuminate\Database\Seeder;

class DosenTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('dosen')->delete();
        
        \DB::table('dosen')->insert(array (
            0 => 
            array (
                'id_dosen' => '196107131986011001',
                'nama' => 'Drs. Slamet Wibawanto, M.T.',
                'email' => 'slamet@um.ac.id',
                'alamat' => 'Blitar',
                'no_telp' => '089896237623',
                'password' => '$2y$10$hn.ORy.KUBZ9m2D5ZXCMze3hexI70lsKBMv1l.j4x.j/pBblDrXwm',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:21:29',
                'updated_at' => '2017-04-12 23:21:31',
            ),
            1 => 
            array (
                'id_dosen' => '196207031991031001',
                'nama' => 'Dr. Ir. H. Syaad Patmantara, M.Pd.',
                'email' => 'syaad@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '081776567576',
                'password' => '$2y$10$kIM8p8LUN8ui39EjyImPkOt4.uYhESp5Lf79Pde/BeFtsjFQA/frC',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:22:46',
                'updated_at' => '2017-04-12 23:22:48',
            ),
            2 => 
            array (
                'id_dosen' => '196509161995121001',
                'nama' => 'Dr. Hakkun Elmunsyah, S.T., M.T.',
                'email' => 'hakkun@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '085765675765',
                'password' => '$2y$10$CJqpZyd7xqJ4rkxLM75BTuHTmx0H1jPz4YOg4g0tH04KQ6A2TL6MS',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 19:52:31',
                'updated_at' => '2017-04-17 19:52:34',
            ),
            3 => 
            array (
                'id_dosen' => '196806041997021001',
                'nama' => 'Dr. Muladi, S.T., M.T.',
                'email' => 'muladi@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '083677867676',
                'password' => '$2y$10$B0SQ3yF1t1OXaEyRQRpAMOk4imvXCNABhdP.2ljUkzhbRgfCdeM4S',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:23:31',
                'updated_at' => '2017-04-12 23:23:34',
            ),
            4 => 
            array (
                'id_dosen' => '196907171998021001',
                'nama' => 'I Made Wirawan, S.T., S.S.T., M.T.',
                'email' => 'made@um.ac.id',
                'alamat' => 'Bali',
                'no_telp' => '085652656565',
                'password' => '$2y$10$rC2DC/KeXRSSOt9z/p0YmettcXMMDOoLr1XGSzJ7VUbfHBHU/uvOS',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:24:02',
                'updated_at' => '2017-04-12 23:24:05',
            ),
            5 => 
            array (
                'id_dosen' => '197206071999032002',
                'nama' => 'Yuni Rahmawati, S.T., M.T.',
                'email' => 'yuni@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '081786786876',
                'password' => '$2y$10$SRoygdIO/8VW4M6HfKeB2O6thsglPG3Kg5PUj0AimTmgM59zaxqr.',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:25:19',
                'updated_at' => '2017-04-12 23:25:21',
            ),
            6 => 
            array (
                'id_dosen' => '197411111999032001',
                'nama' => 'Dyah Lestari, S.T., M.Eng.',
                'email' => 'dyah@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '086767868767',
                'password' => '$2y$10$n6WjxToMxrS5VM23BSR1u.9nv9uJ0ZXX1W6Zoumxcl1xbdolbHS/u',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:24:44',
                'updated_at' => '2017-04-12 23:24:46',
            ),
            7 => 
            array (
                'id_dosen' => '197911142005012004',
                'nama' => 'Dr. Eng. Anik Nur Handayani, S.T., M.T.',
                'email' => 'anik@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '086576537667',
                'password' => '$2y$10$jznpSKwlti5ZUx1qEKTgMe6BOA8B5tfpOPuUpzICVS9pknQk8TTzm',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:26:35',
                'updated_at' => '2017-04-12 23:26:37',
            ),
            8 => 
            array (
                'id_dosen' => '197912182005011001',
                'nama' => 'Aji Prasetya Wibawa, S.T., M.M.T., Ph.d.',
                'email' => 'aji@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '085675776574',
                'password' => '$2y$10$xJvhQGIplTNuw.m.pCx/xuM18WihRBop4OPqPhW.svaD8P8mum3Xi',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:25:57',
                'updated_at' => '2017-04-12 23:25:59',
            ),
            9 => 
            array (
                'id_dosen' => '198008072008121002',
                'nama' => 'M. Zainal Arifin, S.Si., M.Kom.',
                'email' => 'zainal@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '081687678687',
                'password' => '$2y$10$Gl74jWqAYPJAYm7jJqIfHeVx3AC1bMbpa4nPopSeF812alDZLbH1m',
                'remember_token' => NULL,
                'created_at' => '2017-04-13 07:43:25',
                'updated_at' => '2017-04-13 07:43:28',
            ),
            10 => 
            array (
                'id_dosen' => '198108172014041001',
                'nama' => 'Agusta Rahmat Taufani, S.T., M.T.',
                'email' => 'agusta@um.ac.id',
                'alamat' => 'Malang',
                'no_telp' => '084656575757',
                'password' => '$2y$10$Io8/ClwHeOSDpwiIb/RMUefRsjR.7fIhJdKa/PbVHda4XactLrCw2',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 19:41:45',
                'updated_at' => '2017-04-17 19:41:48',
            ),
        ));
        
        
    }
}