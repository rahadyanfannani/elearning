<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MahasiswaTableSeeder::class);
         $this->call(DosenTableSeeder::class);
         $this->call(MatakuliahTableSeeder::class);
         $this->call(KelasFisikTableSeeder::class);
        $this->call(KelasVirtualTableSeeder::class);
        $this->call(AmbilKelasTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        factory(App\User::class, 50)->create();
    }
}
