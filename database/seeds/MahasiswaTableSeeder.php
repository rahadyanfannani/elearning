<?php

use Illuminate\Database\Seeder;

class MahasiswaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mahasiswa')->delete();
        
        \DB::table('mahasiswa')->insert(array (
            0 => 
            array (
                'nim' => '',
                'nama' => '',
                'email' => '',
                'alamat' => '',
                'no_telp' => '',
                'password' => '',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:06:09',
                'updated_at' => '2017-04-17 22:06:07',
            ),
            1 => 
            array (
                'nim' => '140533600650',
                'nama' => 'Widya Yunitha Putri',
                'email' => 'widya@gmail.com',
                'alamat' => 'Ponorogo',
                'no_telp' => '086767868676',
                'password' => '$2y$10$89YsFcN4kaEd87EzkwVUoeP/2lz1So2mOuHpytZBwc0ksKOocICJ2',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:18:07',
                'updated_at' => '2017-04-17 22:18:09',
            ),
            2 => 
            array (
                'nim' => '140533600720',
                'nama' => 'Septi Anggita Kurniawan',
                'email' => 'anggit@gmail.com',
                'alamat' => 'Ponorogo',
                'no_telp' => '089767677676',
                'password' => '$2y$10$KL2LKZ9s4dNovxKTfrasmO0DjHUP1M9hi/BohDMqQY2LbkP4CFYVO',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:08:11',
                'updated_at' => '2017-04-17 22:08:08',
            ),
            3 => 
            array (
                'nim' => '140533600736',
                'nama' => 'Win Rizki Putra Gayo',
                'email' => 'win@gmail.com',
                'alamat' => 'Lumajang',
                'no_telp' => '086778676687',
                'password' => '$2y$10$GUkAzQktBClfOgWSJVZW4eGZQt4gzfBTjOtkTQUClvFWyEZLvlmCu',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:19:00',
                'updated_at' => '2017-04-17 22:19:02',
            ),
            4 => 
            array (
                'nim' => '140533600904',
                'nama' => 'Widya Ayu Kusumaning Ratri',
                'email' => 'ayu@gmail.com',
                'alamat' => 'Ponorogo',
                'no_telp' => '086567526565',
                'password' => '$2y$10$qnypnISba5VXT8H/lSIWz.krY3SBPhA6R6f5CVK4PQ2qG./rnXxfK',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:17:27',
                'updated_at' => '2017-04-17 22:17:32',
            ),
            5 => 
            array (
                'nim' => '140533600917',
                'nama' => 'Tegar Restu Kusuma',
                'email' => 'tegar@gmail.com',
                'alamat' => 'Lombok',
                'no_telp' => '085675768798',
                'password' => '$2y$10$hq.h7mqq9vIJi3dslpCwjOmDrsfbIzS7GmgWxz5ymKBdVvsKTaHfG',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:15:02',
                'updated_at' => '2017-04-17 22:15:04',
            ),
            6 => 
            array (
                'nim' => '140533601016',
                'nama' => 'Yudhawan Anis Shobirin',
                'email' => 'yudha@gmail.com',
                'alamat' => 'Lumajang',
                'no_telp' => '087667526565',
                'password' => '$2y$10$ZfRLoNRUodw3/mHCmTF8z.d/oAyCBKIdjJqbeHaKc5PMtHZU1KHBG',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:19:40',
                'updated_at' => '2017-04-17 22:19:45',
            ),
            7 => 
            array (
                'nim' => '140533601033',
                'nama' => 'Salwatul \'Aisy',
                'email' => 'salwa@gmail.com',
                'alamat' => 'Gresik',
                'no_telp' => '089576565655',
                'password' => '$2y$10$6h42RmzUo4l4aGlTRezZr.lB7p.n9nszGxl47UQ9QzkMk88Wyviv.',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:08:01',
                'updated_at' => '2017-04-17 22:08:03',
            ),
            8 => 
            array (
                'nim' => '140533601422',
                'nama' => 'Singgih Adie Kurniawan',
                'email' => 'singgih@gmail.com',
                'alamat' => 'Banyuwangi',
                'no_telp' => '086767678677',
                'password' => '$2y$10$0YriwFMIhH0IVQliEG2x/uQcI.WPUqAm/WqnH8O/yN7l2cvdh.TEO',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:10:21',
                'updated_at' => '2017-04-17 22:10:20',
            ),
            9 => 
            array (
                'nim' => '140533601476',
                'nama' => 'Syifaul Fauziyah',
                'email' => 'syifa@gmail.com',
                'alamat' => 'Bojonegoro',
                'no_telp' => '085675675676',
                'password' => '$2y$10$828YArgOfcFan9p53Sl4W.3i2qd8kPvDnPiv9O73l6AJh0f2DyO3i',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:13:26',
                'updated_at' => '2017-04-17 22:13:24',
            ),
            10 => 
            array (
                'nim' => '140533601571',
                'nama' => 'Ulya Maisyaroh Ahmad',
                'email' => 'ulya@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '089628664137',
                'password' => '$2y$10$c.ytO5mP38qhAlBo20GZ9.oQyq0Yvh.H4D6/T2D9U0Njb5k2fka/i',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:16:33',
                'updated_at' => '2017-04-17 22:16:39',
            ),
            11 => 
            array (
                'nim' => '140533601703',
                'nama' => 'Siti Kurniawati',
                'email' => 'nia@gmail.com',
                'alamat' => 'Pasuruan',
                'no_telp' => '087687676876',
                'password' => '$2y$10$JpWGQONsNcas3Akcce.dSun2q1GyNVeO26WWoxEmVGQn2FEKC487K',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:12:24',
                'updated_at' => '2017-04-17 22:12:26',
            ),
            12 => 
            array (
                'nim' => '140533601721',
                'nama' => 'Ukhty Aulia Parama',
                'email' => 'ukhty@gmail.com',
                'alamat' => 'Tuban',
                'no_telp' => '085765769712',
                'password' => '$2y$10$Op4Ms3bAGTzFdManACkZqOnWTMllJGm1FNOFmZNlXk30t1fIiYsGC',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:15:47',
                'updated_at' => '2017-04-17 22:15:54',
            ),
            13 => 
            array (
                'nim' => '140533601726',
                'nama' => 'Zakaria',
                'email' => 'zakaria@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '088979878972',
                'password' => '$2y$10$hpj0FwONpMNChkY9ZXS2L.NgrEddI.cg9cf2NeXyK9uvtLDfwb4nG',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:20:28',
                'updated_at' => '2017-04-17 22:20:32',
            ),
            14 => 
            array (
                'nim' => '140533601889',
                'nama' => ' Shelvy Lailatul Fitria',
                'email' => 'shelvy@gmail.com',
                'alamat' => 'Pasuruan',
                'no_telp' => '085576567565',
                'password' => '$2y$10$TZusz545Csmam2Jflk0iieNhwqtsRod/TV70Gcdr7u.sDSMK/KyWm',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:10:15',
                'updated_at' => '2017-04-17 22:10:16',
            ),
            15 => 
            array (
                'nim' => '140533602073',
                'nama' => 'Ronald Evan Barus',
                'email' => 'evan@gmail.com',
                'alamat' => 'Riau',
                'no_telp' => '089576567565',
                'password' => '$2y$10$w7ZCQRhMi4uS/rkgj.5G7u3F3Ulx6NtgzbVbHcMV3a4Qzij646jXK',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:06:02',
                'updated_at' => '2017-04-17 22:06:04',
            ),
            16 => 
            array (
                'nim' => '140533602216',
                'nama' => 'Siti Rohmah',
                'email' => 'aimo@gmail.com',
                'alamat' => 'Bojonegoro',
                'no_telp' => '089989989899',
                'password' => '$2y$10$ilYB1HAYkSJDsvxb6DwKMOraEDtEUio1k7AaAakvrsyfPEjBoUE66',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:13:18',
                'updated_at' => '2017-04-17 22:13:20',
            ),
            17 => 
            array (
                'nim' => '140533602430',
                'nama' => 'Andhik Catur Nugroho',
                'email' => 'andhik@gmail.com',
                'alamat' => 'Nganjuk',
                'no_telp' => '081789798798',
                'password' => '$2y$10$9qudWfksEz1jckUuBA8wfuXCEkKLVwLxtUbYA//n.UIiWzyjDHuQS',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:15:32',
                'updated_at' => '2017-04-12 09:15:27',
            ),
            18 => 
            array (
                'nim' => '140533602472',
                'nama' => 'Nur Fahmi Alfiani',
                'email' => 'fia@gmail.com',
                'alamat' => 'Sidoarjo',
                'no_telp' => '085756757657',
                'password' => '$2y$10$kt3fMYPBAc9qzui5As8cq.ZnO.fwcImy/62mu3NM1ZdSLV3L7winO',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:03:38',
                'updated_at' => '2017-04-17 22:03:41',
            ),
            19 => 
            array (
                'nim' => '140533602478',
                'nama' => 'Rahadyan Fannani Arif',
                'email' => 'fani@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '083788978977',
                'password' => '$2y$10$74zSzTBsc63foZ02DlPFl.Q.HSXX/NUcWiLLJPvStHKC8nH5WPT92',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:04:26',
                'updated_at' => '2017-04-17 22:04:28',
            ),
            20 => 
            array (
                'nim' => '140533602484',
                'nama' => 'Gradiyanto Radityo Kusumo',
                'email' => 'gredy@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '085567126712',
                'password' => '$2y$10$cogU.D1punOZLSMh7C2Q0eq2RIN9lsW21cmfMFROdk8itOdDbhkka',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 23:10:34',
                'updated_at' => '2017-04-12 23:10:37',
            ),
            21 => 
            array (
                'nim' => '140533602574',
                'nama' => 'Risnal Mambrasar',
                'email' => 'risnal@gmail.com',
                'alamat' => 'Papua',
                'no_telp' => '081879867897',
                'password' => '$2y$10$n7lJnC6y0P4XHpLmB1hOjO8lr6kGLWaMNJG4m6.bsER4OAmgIv5kG',
                'remember_token' => NULL,
                'created_at' => '2017-04-17 22:05:12',
                'updated_at' => '2017-04-17 22:05:14',
            ),
            22 => 
            array (
                'nim' => '140533602916',
                'nama' => 'Ana Putri Anggraeni',
                'email' => 'ana@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '081678678687',
                'password' => '$2y$10$s1PWZoUlBbDb.sPUIfLHM.kWYP4p7L52hfBKQfiXVpg12Xg4yXh4W',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:12:04',
                'updated_at' => '2017-04-12 09:11:59',
            ),
            23 => 
            array (
                'nim' => '140533602950',
                'nama' => 'Aguwin Ardi Pranata',
                'email' => 'nata@gmail.com',
                'alamat' => 'Mojokerto',
                'no_telp' => '086687264876',
                'password' => '$2y$10$9RwGxGjx0/QMlfUqRq/sBO6IBfZm/6zx3yr/9NgCwOUL694SeX65a',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:09:46',
                'updated_at' => '2017-04-12 09:09:51',
            ),
            24 => 
            array (
                'nim' => '140533603159',
                'nama' => 'Ancha Aftianto ',
                'email' => 'ancha@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '085762786623',
                'password' => '$2y$10$pQWAkyeWBROj8rS40MApuuplf7V6w0xlmTcC7Gt4wxzvyCCRmpI6i',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:13:33',
                'updated_at' => '2017-04-12 09:13:31',
            ),
            25 => 
            array (
                'nim' => '140533603250',
                'nama' => 'Ade Wicaksono',
                'email' => 'ade@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '081765236523',
                'password' => '$2y$10$M815tt3.WFUc2zOFZAyhTOQ1CHE2M5hfb36PHYfszfwxsexBxCXv.',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:06:25',
                'updated_at' => '2017-04-12 09:06:28',
            ),
            26 => 
            array (
                'nim' => '140533603355',
                'nama' => 'Alipah Nurhidayati',
                'email' => 'alipah@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '085678686868',
                'password' => '$2y$10$UScPP5sCatGGcG.z.hw8T.ltfiuqsbnxW.7f4hbujB8MW4MaCCsim',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:11:52',
                'updated_at' => '2017-04-12 09:11:55',
            ),
            27 => 
            array (
                'nim' => '140533603647',
                'nama' => 'Annisa Rachmawati Putri',
                'email' => 'anisa@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '085766768767',
                'password' => '$2y$10$5cGkmrg8FnwI4MhD3t4rDu52i1LB0f8jIGr4UkM5.M/qV/fgeWJAy',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:17:07',
                'updated_at' => '2017-04-12 09:17:10',
            ),
            28 => 
            array (
                'nim' => '140533603850',
                'nama' => 'Achmad Ghufron',
                'email' => 'gufron@gmail.com',
                'alamat' => 'Purwosari',
                'no_telp' => '085765123667',
                'password' => '$2y$10$yw7TuPlpYAOFzY3GGbOt1u.FhPvqSaswGdLJpV2Y/1pq/Xy8ArMCC',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:05:31',
                'updated_at' => '2017-04-12 09:05:28',
            ),
            29 => 
            array (
                'nim' => '140533604130',
                'nama' => 'Adib Maulana Yusro',
                'email' => 'adib@gmail.com',
                'alamat' => 'Bojonegoro',
                'no_telp' => '086724687237',
                'password' => '$2y$10$d2sStIyseo/SDChUjCDRSe6KdKBF94A/Ap.9f0Vdkl3VyL8WTQ28K',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:07:09',
                'updated_at' => '2017-04-12 09:07:14',
            ),
            30 => 
            array (
                'nim' => '140533604337',
                'nama' => 'Anak Agung Yuniartha',
                'email' => 'agung@gmail.com',
                'alamat' => 'Bali',
                'no_telp' => '086876868768',
                'password' => '$2y$10$I8O1WAnAw1nhM2VUGwKgMuGEhzI1LyalqkSGCzvlPaZQlZRRR/phe',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:12:51',
                'updated_at' => '2017-04-12 09:12:54',
            ),
            31 => 
            array (
                'nim' => '140533604497',
                'nama' => 'Achmad Darmo Joyo ',
                'email' => 'darmo@gmail.com',
                'alamat' => 'Pandaan',
                'no_telp' => '085762374237',
                'password' => '$2y$10$v/0NATAaO2zuQUVqsTUhtOyKrGR9o9GVFyNip4pkYg9MJ/y80asNK',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:05:18',
                'updated_at' => '2017-04-12 09:05:22',
            ),
            32 => 
            array (
                'nim' => '140533604538',
                'nama' => 'Ahmad Lutfi Hidayatulloh',
                'email' => 'hida@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '081876876876',
                'password' => '$2y$10$IkG6gAS1GnK0bhd4tz8g2ulaNeJN4hNFuqxCZw3snX69QnBMKiYwS',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:10:20',
                'updated_at' => '2017-04-12 09:10:16',
            ),
            33 => 
            array (
                'nim' => '140533604892',
                'nama' => 'Abdul Widodo',
                'email' => 'widodo@gmail.com',
                'alamat' => 'Malang',
                'no_telp' => '085678765432',
                'password' => '$2y$10$AzWf/T.ZnVCp3mAqOYXB..HOT24GieTqNc.lWbBcHEXpk3j9RGham',
                'remember_token' => NULL,
                'created_at' => '2017-04-12 09:04:27',
                'updated_at' => '2017-04-12 09:04:30',
            ),
        ));
        
        
    }
}