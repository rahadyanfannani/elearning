<?php

use Illuminate\Database\Seeder;

class MatakuliahTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('matakuliah')->delete();

        \DB::table('matakuliah')->insert(array (
            0 =>
            array (
                'kode_matkul' => '',
                'nama_matkul' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'kode_matkul' => 'FTEK602',
                'nama_matkul' => 'Metodologi Penelitian ',
                'created_at' => '2017-04-09 18:32:02',
                'updated_at' => '2017-04-09 18:32:04',
            ),
            2 =>
            array (
                'kode_matkul' => 'FTEK603',
                'nama_matkul' => 'Kesehatan dan Keselamatan Kerja',
                'created_at' => '2017-04-09 18:32:20',
                'updated_at' => '2017-04-09 18:32:23',
            ),
            3 =>
            array (
                'kode_matkul' => 'FTEK604',
                'nama_matkul' => 'Kewirausahaan',
                'created_at' => '2017-04-09 18:32:41',
                'updated_at' => '2017-04-09 18:32:44',
            ),
            4 =>
            array (
                'kode_matkul' => 'FTEK605',
                'nama_matkul' => 'Kurikulum Pendidikan Kejuruan',
                'created_at' => '2017-04-09 18:33:11',
                'updated_at' => '2017-04-09 18:33:15',
            ),
            5 =>
            array (
                'kode_matkul' => 'FTEK606',
                'nama_matkul' => 'Pengembangan Sumber Belajar ',
                'created_at' => '2017-04-09 18:35:52',
                'updated_at' => '2017-04-09 18:35:55',
            ),
            6 =>
            array (
                'kode_matkul' => 'FTEK608',
                'nama_matkul' => 'Evaluasi Pendidikan ',
                'created_at' => '2017-04-09 18:46:39',
                'updated_at' => '2017-04-09 18:46:40',
            ),
            7 =>
            array (
                'kode_matkul' => 'FTEK609',
                'nama_matkul' => 'Praktik Pembelajaran Mikro',
                'created_at' => '2017-04-09 18:46:58',
                'updated_at' => '2017-04-09 18:47:00',
            ),
            8 =>
            array (
                'kode_matkul' => 'PTIN601',
                'nama_matkul' => 'Manajemen Pendidikan Kejuruan',
                'created_at' => '2017-04-09 18:47:22',
                'updated_at' => '2017-04-09 18:47:23',
            ),
            9 =>
            array (
                'kode_matkul' => 'PTIN602',
                'nama_matkul' => 'Statistik ',
                'created_at' => '2017-04-09 18:47:41',
                'updated_at' => '2017-04-09 18:47:44',
            ),
            10 =>
            array (
                'kode_matkul' => 'PTIN603',
                'nama_matkul' => 'Workshop Pengembangan Perangkat Pembelajaran',
                'created_at' => '2017-04-09 18:48:08',
                'updated_at' => '2017-04-09 18:48:10',
            ),
            11 =>
            array (
                'kode_matkul' => 'PTIN604',
                'nama_matkul' => 'Workshop Pengelolaan Kelas',
                'created_at' => '2017-04-09 18:48:28',
                'updated_at' => '2017-04-09 18:48:30',
            ),
            12 =>
            array (
                'kode_matkul' => 'PTIN605',
                'nama_matkul' => 'Pengantar Teknologi Informasi ',
                'created_at' => '2017-04-09 18:51:02',
                'updated_at' => '2017-04-09 18:51:05',
            ),
            13 =>
            array (
                'kode_matkul' => 'PTIN606',
                'nama_matkul' => 'Matematika Teknik ',
                'created_at' => '2017-04-09 18:51:25',
                'updated_at' => '2017-04-09 18:51:27',
            ),
            14 =>
            array (
                'kode_matkul' => 'PTIN607',
                'nama_matkul' => 'Dasar Pemrograman Komputer ',
                'created_at' => '2017-04-09 18:54:02',
                'updated_at' => '2017-04-09 18:54:04',
            ),
            15 =>
            array (
                'kode_matkul' => 'PTIN608',
                'nama_matkul' => 'Elektronika',
                'created_at' => '2017-04-09 18:54:16',
                'updated_at' => '2017-04-09 18:54:19',
            ),
            16 =>
            array (
                'kode_matkul' => 'PTIN609',
                'nama_matkul' => 'Arsitektur dan Organisasi Komputer  ',
                'created_at' => '2017-04-09 18:54:32',
                'updated_at' => '2017-04-09 18:54:33',
            ),
            17 =>
            array (
                'kode_matkul' => 'PTIN610 ',
                'nama_matkul' => 'Algoritma & Struktur Data ',
                'created_at' => '2017-04-09 18:54:53',
                'updated_at' => '2017-04-09 18:54:55',
            ),
            18 =>
            array (
                'kode_matkul' => 'PTIN611',
                'nama_matkul' => 'Basisdata ',
                'created_at' => '2017-04-09 18:55:10',
                'updated_at' => '2017-04-09 18:55:12',
            ),
            19 =>
            array (
                'kode_matkul' => 'PTIN612',
                'nama_matkul' => 'Digital dan Mikroprosesor ',
                'created_at' => '2017-04-09 18:55:25',
                'updated_at' => '2017-04-09 18:55:27',
            ),
            20 =>
            array (
                'kode_matkul' => 'PTIN613',
                'nama_matkul' => 'Matematika Diskrit ',
                'created_at' => '2017-04-09 18:55:43',
                'updated_at' => '2017-04-09 18:55:44',
            ),
            21 =>
            array (
                'kode_matkul' => 'PTIN614 ',
                'nama_matkul' => 'Bahasa Inggris Teknik I  ',
                'created_at' => '2017-04-09 18:55:56',
                'updated_at' => '2017-04-09 18:55:58',
            ),
            22 =>
            array (
                'kode_matkul' => 'PTIN615 ',
                'nama_matkul' => 'Bahasa Inggris Teknik II',
                'created_at' => '2017-04-09 18:56:16',
                'updated_at' => '2017-04-09 18:56:18',
            ),
            23 =>
            array (
                'kode_matkul' => 'PTIN616',
                'nama_matkul' => 'Sistem Informasi ',
                'created_at' => '2017-04-09 18:56:38',
                'updated_at' => '2017-04-09 18:56:40',
            ),
            24 =>
            array (
                'kode_matkul' => 'PTIN617',
                'nama_matkul' => 'Sistem Operasi ',
                'created_at' => '2017-04-09 18:59:26',
                'updated_at' => '2017-04-09 18:59:28',
            ),
            25 =>
            array (
                'kode_matkul' => 'PTIN618',
                'nama_matkul' => 'Grafika Komputer ',
                'created_at' => '2017-04-09 18:59:43',
                'updated_at' => '2017-04-09 18:59:44',
            ),
            26 =>
            array (
                'kode_matkul' => 'PTIN619',
                'nama_matkul' => 'Komunikasi data dan jaringan komputer ',
                'created_at' => '2017-04-09 19:03:27',
                'updated_at' => '2017-04-09 19:03:29',
            ),
            27 =>
            array (
                'kode_matkul' => 'PTIN620',
                'nama_matkul' => 'Pemrograman Visual',
                'created_at' => '2017-04-09 19:03:46',
                'updated_at' => '2017-04-09 19:03:47',
            ),
            28 =>
            array (
                'kode_matkul' => 'PTIN621',
                'nama_matkul' => 'Perawatan dan Perbaikan Komputer',
                'created_at' => '2017-04-09 19:06:38',
                'updated_at' => '2017-04-09 19:06:40',
            ),
            29 =>
            array (
                'kode_matkul' => 'PTIN622 ',
                'nama_matkul' => 'Pemrograman Berbasis Web ',
                'created_at' => '2017-04-09 19:06:56',
                'updated_at' => '2017-04-09 19:06:57',
            ),
            30 =>
            array (
                'kode_matkul' => 'PTIN623',
                'nama_matkul' => 'Analisis dan Desain Sistem Informasi',
                'created_at' => '2017-04-09 19:07:22',
                'updated_at' => '2017-04-09 19:07:24',
            ),
            31 =>
            array (
                'kode_matkul' => 'PTIN624 ',
                'nama_matkul' => 'Multimedia',
                'created_at' => '2017-04-09 19:07:36',
                'updated_at' => '2017-04-09 19:07:37',
            ),
            32 =>
            array (
                'kode_matkul' => 'PTIN625 ',
                'nama_matkul' => ' Rekayasa Perangkat Lunak ',
                'created_at' => '2017-04-09 19:07:55',
                'updated_at' => '2017-04-09 19:07:57',
            ),
            33 =>
            array (
                'kode_matkul' => 'PTIN626',
                'nama_matkul' => 'Keamanan Sistem komputer ',
                'created_at' => '2017-04-09 19:08:18',
                'updated_at' => '2017-04-09 19:08:20',
            ),
            34 =>
            array (
                'kode_matkul' => 'PTIN627',
                'nama_matkul' => 'Komputasi Numerik',
                'created_at' => '2017-04-09 19:08:34',
                'updated_at' => '2017-04-09 19:08:35',
            ),
            35 =>
            array (
                'kode_matkul' => 'PTIN628 ',
                'nama_matkul' => 'Bahasa Inggris Teknik II b',
                'created_at' => '2017-04-09 19:09:01',
                'updated_at' => '2017-04-09 19:09:03',
            ),
            36 =>
            array (
                'kode_matkul' => 'PTIN629',
                'nama_matkul' => 'Pembelajaran Berbantuan TIK ',
                'created_at' => '2017-04-09 19:09:49',
                'updated_at' => '2017-04-09 19:09:50',
            ),
            37 =>
            array (
                'kode_matkul' => 'PTIN630 ',
                'nama_matkul' => 'Kecerdasan Buatan  ',
                'created_at' => '2017-04-09 19:10:04',
                'updated_at' => '2017-04-09 19:10:05',
            ),
            38 =>
            array (
                'kode_matkul' => 'PTIN631',
                'nama_matkul' => 'Dicision Support System dan Business Intelligence ',
                'created_at' => '2017-04-09 19:11:45',
                'updated_at' => '2017-04-09 19:11:46',
            ),
            39 =>
            array (
                'kode_matkul' => 'PTIN632',
                'nama_matkul' => 'Mobile Learning ',
                'created_at' => '2017-04-09 19:12:18',
                'updated_at' => '2017-04-09 19:12:20',
            ),
            40 =>
            array (
                'kode_matkul' => 'PTIN633',
                'nama_matkul' => 'Game Edukasi ',
                'created_at' => '2017-04-09 19:12:44',
                'updated_at' => '2017-04-09 19:12:45',
            ),
            41 =>
            array (
                'kode_matkul' => 'PTIN634',
                'nama_matkul' => 'Pengolahan Citra ',
                'created_at' => '2017-04-09 19:13:04',
                'updated_at' => '2017-04-09 19:13:06',
            ),
            42 =>
            array (
                'kode_matkul' => 'PTIN635',
                'nama_matkul' => 'Basisdata Terdistribusi ',
                'created_at' => '2017-04-09 19:13:34',
                'updated_at' => '2017-04-09 19:13:36',
            ),
            43 =>
            array (
                'kode_matkul' => 'PTIN636',
                'nama_matkul' => 'Interaksi Manusia dan Komputer ',
                'created_at' => '2017-04-09 19:13:49',
                'updated_at' => '2017-04-09 19:13:51',
            ),
            44 =>
            array (
                'kode_matkul' => 'PTIN637',
                'nama_matkul' => 'Animasi Pembelajaran ',
                'created_at' => '2017-04-09 19:14:09',
                'updated_at' => '2017-04-09 19:14:11',
            ),
            45 =>
            array (
                'kode_matkul' => 'PTIN638',
                'nama_matkul' => 'e-Bisnis  ',
                'created_at' => '2017-04-09 19:14:24',
                'updated_at' => '2017-04-09 19:14:25',
            ),
            46 =>
            array (
                'kode_matkul' => 'PTIN639',
                'nama_matkul' => 'Integrasi System ',
                'created_at' => '2017-04-09 19:14:39',
                'updated_at' => '2017-04-09 19:14:41',
            ),
            47 =>
            array (
                'kode_matkul' => 'PTIN640',
                'nama_matkul' => 'Workshop Jaringan Komputer ',
                'created_at' => '2017-04-09 19:18:11',
                'updated_at' => '2017-04-09 19:18:12',
            ),
            48 =>
            array (
                'kode_matkul' => 'PTIN641',
                'nama_matkul' => 'Workshop Perangkat Lunak ',
                'created_at' => '2017-04-09 19:18:33',
                'updated_at' => '2017-04-09 19:18:34',
            ),
            49 =>
            array (
                'kode_matkul' => 'PTIN642',
                'nama_matkul' => 'Workshop Multimedia ',
                'created_at' => '2017-04-09 19:18:53',
                'updated_at' => '2017-04-09 19:18:54',
            ),
            50 =>
            array (
                'kode_matkul' => 'PTIN643',
                'nama_matkul' => 'Praktik Industri ',
                'created_at' => '2017-04-09 19:19:32',
                'updated_at' => '2017-04-09 19:19:33',
            ),
            51 =>
            array (
                'kode_matkul' => 'PTIN644',
            'nama_matkul' => 'Seminar Praskripsi (Tata Tulis dan Seminar) ',
                'created_at' => '2017-04-09 19:19:50',
                'updated_at' => '2017-04-09 19:19:52',
            ),
            52 =>
            array (
                'kode_matkul' => 'PTIN645',
                'nama_matkul' => 'Skripsi ',
                'created_at' => '2017-04-09 19:20:08',
                'updated_at' => '2017-04-09 19:20:10',
            ),
            53 =>
            array (
                'kode_matkul' => 'PTIN646',
                'nama_matkul' => 'Pemrograman Berorientasi Objek ',
                'created_at' => '2017-04-09 18:56:59',
                'updated_at' => '2017-04-09 18:57:00',
            ),
            54 =>
            array (
                'kode_matkul' => 'UKPL601',
                'nama_matkul' => 'Kajian dan Praktik Keguruan  ',
                'created_at' => '2017-04-09 19:19:10',
                'updated_at' => '2017-04-09 19:19:14',
            ),
            55 =>
            array (
                'kode_matkul' => 'UMKK601',
                'nama_matkul' => 'Pengantar Pendidikan',
                'created_at' => '2017-04-09 18:19:52',
                'updated_at' => '2017-04-09 18:19:55',
            ),
            56 =>
            array (
                'kode_matkul' => 'UMKK602',
                'nama_matkul' => 'Perkembangan Peserta Didik

',
                'created_at' => '2017-04-09 18:23:20',
                'updated_at' => '2017-04-09 18:23:23',
            ),
            57 =>
            array (
                'kode_matkul' => 'UMKK603',
                'nama_matkul' => 'Belajar dan Pembelajaran',
                'created_at' => '2017-04-09 18:23:49',
                'updated_at' => '2017-04-09 18:23:51',
            ),
            58 =>
            array (
                'kode_matkul' => 'UMPK601',
                'nama_matkul' => 'Pendidikan Agama Islam',
                'created_at' => '2017-04-09 10:28:00',
                'updated_at' => '2017-04-09 10:28:00',
            ),
            59 =>
            array (
                'kode_matkul' => 'UMPK602',
                'nama_matkul' => 'Pendidikan Agama Protestan',
                'created_at' => '2017-04-09 10:28:00',
                'updated_at' => '2017-04-09 10:28:00',
            ),
            60 =>
            array (
                'kode_matkul' => 'UMPK603',
                'nama_matkul' => 'Pendidikan Agama Katolik',
                'created_at' => '2017-04-09 10:28:00',
                'updated_at' => '2017-04-09 10:28:00',
            ),
            61 =>
            array (
                'kode_matkul' => 'UMPK604 ',
                'nama_matkul' => 'Pendidikan Agama Hindu',
                'created_at' => '2017-04-09 18:16:06',
                'updated_at' => '2017-04-09 18:16:22',
            ),
            62 =>
            array (
                'kode_matkul' => 'UMPK605',
                'nama_matkul' => 'Pendidikan Agama Budha',
                'created_at' => '2017-04-09 18:16:11',
                'updated_at' => '2017-04-09 18:16:24',
            ),
            63 =>
            array (
                'kode_matkul' => 'UMPK606',
                'nama_matkul' => 'Pendidikan Pancasila ',
                'created_at' => '2017-04-09 18:17:20',
                'updated_at' => '2017-04-09 18:17:23',
            ),
            64 =>
            array (
                'kode_matkul' => 'UMPK607',
                'nama_matkul' => 'Pendidikan Kewarganegaraan',
                'created_at' => '2017-04-09 18:18:05',
                'updated_at' => '2017-04-09 18:18:07',
            ),
            65 =>
            array (
                'kode_matkul' => 'UMPK608',
                'nama_matkul' => 'Bahasa Indonesia Keilmuan',
                'created_at' => '2017-04-09 18:18:32',
                'updated_at' => '2017-04-09 18:18:39',
            ),
        ));


    }
}
