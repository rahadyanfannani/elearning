<?php

use Illuminate\Database\Seeder;

class AmbilKelasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ambil_kelas')->delete();
        
        \DB::table('ambil_kelas')->insert(array (
            0 => 
            array (
                'id_ambil_kelas' => 14014011,
                'id_kelas_virtual' => 13014011,
                'nim' => '140533602430',
                'status' => '1',
                'created_at' => '2017-04-13 07:50:56',
                'updated_at' => '2017-04-13 07:50:58',
            ),
            1 => 
            array (
                'id_ambil_kelas' => 14014012,
                'id_kelas_virtual' => 13014012,
                'nim' => '140533602484',
                'status' => '1',
                'created_at' => '2017-04-13 07:52:52',
                'updated_at' => '2017-04-13 07:52:54',
            ),
            2 => 
            array (
                'id_ambil_kelas' => 14014013,
                'id_kelas_virtual' => 13014013,
                'nim' => '140533602916',
                'status' => '1',
                'created_at' => '2017-04-13 08:07:45',
                'updated_at' => '2017-04-13 08:07:48',
            ),
            3 => 
            array (
                'id_ambil_kelas' => 14014014,
                'id_kelas_virtual' => 13014013,
                'nim' => '140533602950',
                'status' => '1',
                'created_at' => '2017-04-13 08:08:20',
                'updated_at' => '2017-04-13 08:08:22',
            ),
            4 => 
            array (
                'id_ambil_kelas' => 14014015,
                'id_kelas_virtual' => 13014012,
                'nim' => '140533603159',
                'status' => '1',
                'created_at' => '2017-04-13 08:08:58',
                'updated_at' => '2017-04-13 08:09:01',
            ),
            5 => 
            array (
                'id_ambil_kelas' => 14014016,
                'id_kelas_virtual' => 13014011,
                'nim' => '140533603250',
                'status' => '1',
                'created_at' => '2017-04-13 08:09:35',
                'updated_at' => '2017-04-13 08:09:38',
            ),
            6 => 
            array (
                'id_ambil_kelas' => 14014017,
                'id_kelas_virtual' => 13014011,
                'nim' => '140533603355',
                'status' => '1',
                'created_at' => '2017-04-13 08:10:18',
                'updated_at' => '2017-04-13 08:10:20',
            ),
            7 => 
            array (
                'id_ambil_kelas' => 14014018,
                'id_kelas_virtual' => 13014011,
                'nim' => '140533603647',
                'status' => '0',
                'created_at' => '2017-04-13 08:11:32',
                'updated_at' => '2017-04-13 08:11:35',
            ),
        ));
        
        
    }
}