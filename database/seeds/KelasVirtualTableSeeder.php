<?php

use Illuminate\Database\Seeder;

class KelasVirtualTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('kelas_virtual')->delete();
        
        \DB::table('kelas_virtual')->insert(array (
            0 => 
            array (
                'id_kelas_virtual' => 13014011,
                'id_kelas' => '11014013',
                'id_dosen' => '197911142005012004',
                'kode_matkul' => 'PTIN614 ',
                'created_at' => '2017-04-12 23:40:31',
                'updated_at' => '2017-04-12 23:40:34',
            ),
            1 => 
            array (
                'id_kelas_virtual' => 13014012,
                'id_kelas' => '11014013',
                'id_dosen' => '196107131986011001',
                'kode_matkul' => 'PTIN616',
                'created_at' => '2017-04-13 07:34:48',
                'updated_at' => '2017-04-13 07:34:51',
            ),
            2 => 
            array (
                'id_kelas_virtual' => 13014013,
                'id_kelas' => '11014013',
                'id_dosen' => '196907171998021001',
                'kode_matkul' => 'FTEK603',
                'created_at' => '2017-04-13 08:26:41',
                'updated_at' => '2017-04-13 08:26:44',
            ),
            3 => 
            array (
                'id_kelas_virtual' => 13014014,
                'id_kelas' => '11014013',
                'id_dosen' => '197912182005011001',
                'kode_matkul' => 'FTEK606',
                'created_at' => '2017-04-13 08:28:18',
                'updated_at' => '2017-04-13 08:28:20',
            ),
            4 => 
            array (
                'id_kelas_virtual' => 13014015,
                'id_kelas' => '11014013',
                'id_dosen' => '197411111999032001',
                'kode_matkul' => 'UMPK608',
                'created_at' => '2017-04-17 19:49:29',
                'updated_at' => '2017-04-17 19:49:32',
            ),
            5 => 
            array (
                'id_kelas_virtual' => 13014016,
                'id_kelas' => '11014013',
                'id_dosen' => '196509161995121001',
                'kode_matkul' => 'PTIN601',
                'created_at' => '2017-04-17 19:53:03',
                'updated_at' => '2017-04-17 19:53:06',
            ),
            6 => 
            array (
                'id_kelas_virtual' => 13014017,
                'id_kelas' => '11014013',
                'id_dosen' => '196207031991031001',
                'kode_matkul' => 'PTIN646',
                'created_at' => '2017-04-17 19:55:12',
                'updated_at' => '2017-04-17 19:55:15',
            ),
            7 => 
            array (
                'id_kelas_virtual' => 13014018,
                'id_kelas' => '11014013',
                'id_dosen' => '196806041997021001',
                'kode_matkul' => 'UMPK606',
                'created_at' => '2017-04-17 19:55:55',
                'updated_at' => '2017-04-17 19:55:57',
            ),
            8 => 
            array (
                'id_kelas_virtual' => 13014019,
                'id_kelas' => '11014013',
                'id_dosen' => '198108172014041001',
                'kode_matkul' => 'PTIN621',
                'created_at' => '2017-04-17 19:58:15',
                'updated_at' => '2017-04-17 19:58:17',
            ),
        ));
        
        
    }
}