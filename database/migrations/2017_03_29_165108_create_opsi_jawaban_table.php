<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpsiJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('opsi_jawaban',function(Blueprint $table){
        $table->increments('id_opsi_jawaban');
        $table->integer('id_item');
        $table->string('a');
        $table->string('b');
        $table->string('c');
        $table->string('d');
        $table->string('e');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opsi_jawaban');
    }
}
