<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPertemuan extends Model
{
    protected $table = "detail_pertemuan";

    function konten(){
        if($this->tipe == "materi"){
            return $this->hasOne('App\Materi',"id","id_konten");
        } else if($this->tipe == "evaluasi"){
            return $this->hasOne('App\Evaluasi',"id","id_konten");
        } else if($this->tipe == "essay"){
            return $this->hasOne('App\Essay',"id","id_konten");
        }
    }

}
