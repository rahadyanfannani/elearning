<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluasi extends Model
{
    protected $table = "evaluasi";

    function soal(){

            return $this->hasMany('App\Soal',"id_evaluasi","id");

    }
}
