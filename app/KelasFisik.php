<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelasFisik extends Model
{
  protected $table = "kelas_fisik";
  protected $primaryKey = "id_kelas";

  function KelasFisik(){
    return $this->haveMany("App\KelasVirtual","id_kelas","id_kelas");
  }
}
