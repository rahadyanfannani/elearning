<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertemuan extends Model
{
    protected $table = "pertemuan";
    protected $primaryKey = "id_pertemuan";

    function kelasVirtual(){
      return $this->belongsTo("App\KelasVirtual","id_kelas_virtual","id_kelas_virtual");
    }

    function detail(){
        return $this->hasMany("App\DetailPertemuan","id_pertemuan","id_pertemuan");
    }


}
