<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmbilKelas extends Model
{
  protected $table = "ambil_kelas";
    protected $primaryKey = "id_ambil_kelas";

  function kelasVirtual(){
    return $this->belongsTo("App\KelasVirtual","id_kelas_virtual","id_kelas_virtual");
  }
  function mahasiswa(){
  return $this->belongsTo("App\Mahasiswa","nim","nim");
  }

}
