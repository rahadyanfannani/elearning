<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
  protected $table = "matakuliah";
  protected $primaryKey = "kode_matkul";
  public $incrementing = false;

  function KelasVirtual(){
    return $this->haveMany("App\KelasVirtual","kode_matkul","kode_matkul");
  }
  function materi(){
    return $this->haveMany("App\Materi","kode_matkul","kode_matkul");
}
}
