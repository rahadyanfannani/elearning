<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
   protected $table = "materi";

   function dokumen(){
     return $this->hasMany("App\Dokumen","id_materi","id");
   }

   function pertemuan(){
     return $this->belongsTo("App/Pertemuan","id_pertemuan","id_pertemuan");
   }
}
