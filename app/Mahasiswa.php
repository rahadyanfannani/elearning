<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Mahasiswa  extends Authenticatable
{
    use Notifiable;


    protected $table = "mahasiswa";
    protected $primaryKey = "nim";
    public $incrementing = false;

    function ambilKelas(){
      return $this->hasMany('App\AmbilKelas','nim', 'nim');
    }
}
