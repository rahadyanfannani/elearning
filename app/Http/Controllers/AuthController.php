<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Mahasiswa;
use App\Dosen;


class AuthController extends Controller
{
    function loginMahasiswa(Request $request){
      return view('mahasiswa.login');
    }
    function loginDosen(Request $request){
      return view('dosen.login');
    }
    function doLoginDosen($nip, $password){
        $dosen = Dosen::find($nip);
      if($dosen){
        if (Hash::check($password, $dosen->password)) {
          return true;
        }
      } else {
          return false;
      }
    }
    function postLoginDosen(Request $request){
      $this->id_dosen = $request->nip;
      $this->password = $request->password;
      $validator = Validator::make($request->all(), [

         'nip' => 'required',
         'password' => 'required'
        ]);
        $validator->after(function ($validator) {
            if (!$this->doLoginDosen($this->id_dosen,$this->password)) {
                $validator->errors()->add('nip', 'NIP atau password anda salah!');
            }
        });

        if ($validator->fails()) {
            return redirect('dosen/login')
                        ->withErrors($validator);
        } else {
            session(['id_dosen' => $this->id_dosen, 'status' => 1]);

            return redirect('dosen/dashboard');
        }
    }

    function doLoginMahasiswa($nim, $password){
        $mahasiswa = Mahasiswa::find($nim);
      if($mahasiswa){
        if (Hash::check($password, $mahasiswa->password)) {
          return true;
        }
      } else {
          return false;
      }
    }
    function postLoginMahasiswa(Request $request){
      $this->nim = $request->nim;
      $this->password = $request->password;
      $validator = Validator::make($request->all(), [

         'nim' => 'required',
         'password' => 'required'
        ]);
        $validator->after(function ($validator) {
            if (!$this->doLoginMahasiswa($this->nim,$this->password)) {
                $validator->errors()->add('nim', 'NIM atau password anda salah!');
            }
        });

        if ($validator->fails()) {
            return redirect('mahasiswa/login')
                        ->withErrors($validator);
        } else {
            session(['nim' => $this->nim, 'status' => 1]);

            return redirect('mahasiswa/dashboard');
        }
    }
}
