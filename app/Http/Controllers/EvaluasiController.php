<?php

namespace App\Http\Controllers;

use App\DetailPertemuan;
use App\Evaluasi;
use Illuminate\Http\Request;


class EvaluasiController extends Controller
{
    function evaluasi(Request $request,$id_evaluasi){
        $data['id_evaluasi'] = $id_evaluasi;
        return view("evaluasi.main",$data);
    }
    function postEssay(Request $request){

    }
    function postEvaluasi(Request $request,$id_evaluasi){
        $evaluasi = DetailPertemuan::find($id_evaluasi);
        $benar = 0;
        $total = 0;
        foreach($evaluasi->konten->soal as $server){
            foreach($request->soal as $client){

                if($server->id == $client['id_soal']){
                    $total++;
                    if( $client['terjawab'] == $server->jawaban){
                        $benar++;
                    }

                }
            }

        }
        $data['nilai'] = round(100 * ($benar/$total));
        return $data;

    }
    function getEvaluasi(Request $request,$id_evaluasi){
        $evaluasi = DetailPertemuan::find($id_evaluasi);

        $data = [];



        $data["nama"] = $evaluasi->konten->nama;
        $data["soal"] = [];
        foreach($evaluasi->konten->soal as $key => $val){
            $soal["id"] = $key;
            $soal["id_soal"] = $val->id;
            $soal["soal"] = $val->pertanyaan;
            $soal["jawaban"] = [];
            $soal['terjawab'] = -1;
            foreach($val->jawabanList as $val2){
                array_push($soal["jawaban"],$val2->jawaban);
            }

            array_push($data['soal'],$soal);
        }
        return $data;
    }
}
