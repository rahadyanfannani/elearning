<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }
    public function home(){
        return view('home');
    }
    public function activate(){

        return redirect('login')->with('status','You are now confirmed. Please login');
    }
    public function upload(){
        return view('upload');
    }
    public function postUpload(Request $request){
        $request->fileupload->storeAs('wpl',$request->fileupload->getClientOriginalName());
        echo "FILE BERHASIL DIKIRIM";
    }

}
