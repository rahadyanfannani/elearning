<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\KelasFisik;
use App\KelasVirtual;
use App\Matakuliah;
use App\Dosen;


class AdminController extends Controller
{
    use AuthenticatesUsers;
    protected function guard()
    {
        return Auth::guard('admin');
    }
    protected function redirectTo(){
        return '/admin/home';
    }

    public function username()
    {
        return 'email';
    }
    function authenticated(Request $request, $user)
    {
        session(['usertype' => 3]);

    }
    function showLoginForm(){
        return view('admin.login');
    }
    function home()
    {
      return view("admin.home");
    }
    function kelas()
    {
      $data["KelasFisik"]=KelasFisik::all();
      return view("admin.kelas",$data);
    }
    function save_kelas(Request $request)
    {
      $kelas=new KelasFisik;
      $kelas->id_kelas=$request->id_kelas;
      $kelas->nama_kelas=$request->nama_kelas;
      $kelas->id_prodi="12014012";
      $status=$request->action;
      if ($status==0) {
        $kelas->save();
      }else {
        KelasFisik::where("id_kelas",$request->id_kelas)->update(["nama_kelas" => $request->nama_kelas]);
      }
      return redirect("admin/kelas");
    }
    function delete_kelas($id){
      KelasFisik::where("id_kelas",$id)->delete();
      return redirect("admin/kelas");
    }
    function kelas_virtual()
    {
      $data["KelasFisik"]=KelasFisik::all();
      $data["matakuliah"]=Matakuliah::all();
      $data["KelasVirtual"]=KelasVirtual::all();
      $data["dosen"]=Dosen::all();
      return view('admin.kelas_virtual',$data);
    }
    function save_kelas_virtual(Request $request)
    {
      $kelas=new KelasVirtual;
      $kelas->id_kelas_virtual=$request->id_kelas_virtual;
      $kelas->kode_matkul=$request->kode_matkul;
      $kelas->id_dosen=$request->id_dosen;
      $kelas->id_kelas=$request->id_kelas;
      $status=$request->action;
      if ($status==0) {
        $kelas->save();
      }else {
        KelasVirtual::where("id_kelas_virtual",$request->id_kelas_virtual)->update(["kode_matkul" => $request->kode_matkul,"id_dosen" => $request->id_dosen,"id_kelas" => $request->id_kelas]);
      }
      return redirect("admin/kelas_virtual");
    }
    function delete_kelas_virtual($id)
    {
      KelasVirtual::where("id_kelas_virtual",$id)->delete();
      return redirect("admin/kelas_virtual");
    }
    function matakuliah()
    {
      $data["matakuliah"]=Matakuliah::all();
      return view("admin.matakuliah",$data);
    }
    function save_matakuliah(Request $request)
    {
      $status=$request->action;
      if ($status==0) {
        $matakuliah=new Matakuliah;
      }else {
        $matakuliah=Matakuliah::find($request->kode_matkul);
      }
      $matakuliah->kode_matkul=$request->kode_matkul;
      $matakuliah->nama_matkul=$request->nama_matkul;
      $matakuliah->save();
      return redirect("admin/matakuliah");
    }
    function delete_matakuliah($kode)
    {
      Matakuliah::where("kode_matkul",$kode)->delete();
      return redirect("admin/matakuliah");
    }
    function dosen()
    {
      $data["dosen"]=Dosen::all();
      return view("admin.dosen",$data);
    }
    function save_dosen(Request $request)
    {
      $status=$request->action;
      if ($status==0) {
        $dosen=new Dosen;
        $dosen->password=bcrypt($request->password);
      }else {
        $dosen=Dosen::find($request->id_dosen);
      }
      $dosen->id_dosen=$request->id_dosen;
      $dosen->nama=$request->nama;
      $dosen->email=$request->email;
      $dosen->alamat=$request->alamat;
      $dosen->no_telp=$request->no_telp;
      $dosen->save();
      return redirect("admin/dosen");
    }
    function delete_dosen($id)
    {
      Dosen::where("id_dosen",$id)->delete();
      return redirect("admin/dosen");
    }
}
