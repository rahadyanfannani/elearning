<?php

namespace App\Http\Controllers;

use App\DetailPertemuan;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Matakuliah;
use App\KelasFisik;
use App\KelasVirtual;
use App\Mahasiswa;
use App\AmbilKelas;
use App\Pertemuan;
use App\Materi;
use Illuminate\Support\Facades\Auth;
class MahasiswaController extends Controller
{

  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

    protected function guard()
    {
        return Auth::guard('mahasiswa');
    }
   public function username()
    {
        return 'nim';
    }

    public function showLoginForm()
      {
          return view('mahasiswa.login');
      }

  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
   protected $redirectTo = '/mahasiswa/dashboard';

    function authenticated(Request $request, $user)
    {
        session(['usertype' => 1]);
    }

    function dashboard(Request $request){

      $data["matakuliah"]=Matakuliah::all();
      $data["kelas"]=KelasFisik::all();
      $data["ambil_kelas"]= Mahasiswa::find(Auth::guard('mahasiswa')->id())->ambilKelas;
      return view('mahasiswa.dashboard',$data);
    }
    function getKelas($kode)
    {
      $kode_matkul=$kode;
      $kelas=KelasVirtual::where("kode_matkul",$kode_matkul)->get();
      //return $kode_matkul;
      if ($kelas->count()!=0) {
        echo "<div class=\"wrap courses-keyword\" ><select name='kelas' required='required' class='select-field categories-courses'>";
        foreach ($kelas as $key => $value) {
          echo "<option value='$value->id_kelas'>".$value->kelas->nama_kelas."</option>";
        }
        echo "</select></div><div class=\"wrap all-categories\">";
        echo '&nbsp;&nbsp;&nbsp;&nbsp;<button>Tambah</button></div>';
      }else {
        echo "<div class='btn btn-danger btn-sm'>Kelas tidak tersedia.</div>";
      }
    }
    function tambahKelas(Request $request){
      $idKelas = $request->kelas;
      $idMatkul= $request->matakuliah;
      $kelasVirtual= KelasVirtual::where("id_kelas","=",$idKelas)->where("kode_matkul","=",$idMatkul)->first();

      $ambil=new AmbilKelas;
      $ambil->id_kelas_virtual=$kelasVirtual->id_kelas_virtual;
      $ambil->nim=Auth::id();
      $ambil->status="0";
      $ambil->save();
      return redirect('mahasiswa/dashboard');
    }
    function list_pertemuan($id){
      $data["kelas"]=KelasVirtual::where("id_kelas_virtual",$id)->first();
      $data["pertemuan"]=Pertemuan::where("id_kelas_virtual","=",$id)->orderBy("urutan")->get();
      $data["jumlah"] = Pertemuan::where("id_kelas_virtual","=",$id)->count();
      return view("mahasiswa.list_pertemuan",$data);
    }
    function materi($idk,$id){
      $data["kelas"]=KelasVirtual::where("id_kelas_virtual",$idk)->first();
      $pertemuan = Pertemuan::find($id);
      $data["pertemuan"]= $pertemuan;
      $data["detail"] = DetailPertemuan::where("id_pertemuan",$pertemuan->id_pertemuan)->orderBy("urutan")->get();
      //echo asset('upload/content_utama/2.mp4');
      //echo file_exists(public_path('upload/content_utama/2.mp4'));
      return view('mahasiswa.materi',$data);
    }
}
