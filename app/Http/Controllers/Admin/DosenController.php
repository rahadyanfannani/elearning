<?php
namespace App\Http\Controllers\Admin;

use App\Dosen;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class DosenController extends Controller
{
  function home(){
    $data['dosen'] = Dosen::all();
    return view('dosen_admin.home',$data);
  }
  function tambah(){
    return view('dosen_admin.tambah');
  }

  function postTambah(Request $request){
      $baru = new Dosen;
      $baru->id_dosen = $request->id_dosen;
      $baru->nama = $request->nama;
      $baru->email  = $request->email;
      $baru->alamat = $request->alamat;
      $baru->no_telp  = $request->no_telp;
      $baru->password = bcrypt($request->password);
      $baru->save();
      return redirect('admin/dosen');
  }

  function edit($id_dosen){
    $edit['dosen']  = Dosen::find($id_dosen);
    return view('dosen_admin.edit',$edit);
  }

  function postedit($id_dosen, Request $request){
    $baruEdit = Dosen::find($id_dosen);
    $baruEdit->id_dosen = $request->id_dosen;
    $baruEdit->nama = $request->nama;
    $baruEdit->email  = $request->email;
    $baruEdit->alamat = $request->alamat;
    $baruEdit->no_telp  = $request->no_telp;
    $baruEdit->password = bcrypt($request->password);
    $baruEdit->save();
    return redirect('admin/dosen');
  }

  function hapus($id_dosen){
    $hapus = Dosen::find($id_dosen);
    $hapus->delete();
    return redirect('admin/dosen');
  }
}


 ?>
