<?php

namespace App\Http\Controllers\Admin;

use App\Matakuliah;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MatakuliahController extends Controller
{
    function home(){
      $data['matkul'] = Matakuliah::all();
      return view('matakuliah.home',$data);
    }
    function tambah(){
      return view('matakuliah.tambah');
    }

    function postTambah(Request $request){
        $baru = new Matakuliah;
        $baru->kode_matkul = $request->kode_matkul;
        $baru->nama_matkul = $request->nama_matkul;
        $baru->save();
        return redirect('admin/matakuliah');
    }

    function edit($kode_matkul){
      $edit['matakuliah']  = Matakuliah::find($kode_matkul);
      return view('matakuliah.edit',$edit);
    }

    function postedit($kode_matkul, Request $request){
      $baruEdit = Matakuliah::find($kode_matkul);
      $baruEdit->kode_matkul = $request->kode_matkul;
      $baruEdit->nama_matkul = $request->nama_matkul;
      $baruEdit->save();
      return redirect('admin/matakuliah');
    }

    function hapus($kode_matkul){
      $hapus = Matakuliah::find($kode_matkul);
      $hapus->delete();
      return redirect('admin/matakuliah');
    }
    }


 ?>
