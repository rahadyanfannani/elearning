<?php

namespace App\Http\Controllers;

use App\KelasVirtual;
use Illuminate\Http\Request;
use App\Pesan;
use App\Events\NewMessage;
use App\Events\OrderShipped;
use App\Model\PesanBroadcast;
use App\Mahasiswa;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    function beranda($id_kelas){
        $data['kelas'] = KelasVirtual::find($id_kelas);
      return view("forum.beranda", $data);

    }
    
    function createMessage($id_kelas,Request $request){
      $pesan = new Pesan;
      $pesan->pesan = $request->pesan;
      $pesan->id_kelas = $id_kelas;
      $pesan->id_user = Auth::id();
      $pesan->status_user = 1;
      $pesan->save();
      $pesanBroadcast = new PesanBroadcast;
      $pesanBroadcast->pesan = $request->pesan;
      $pesanBroadcast->id_kelas = $id_kelas;
      $pesanBroadcast->username = Mahasiswa::find(Auth::id())->nama;
      event(new NewMessage($pesanBroadcast));
      return response()->json($pesanBroadcast);
    }
    function getMessage($id_kelas,Request $request){
      $pesan = Pesan::getPesanBroadcast($id_kelas);
    return $pesan;
    }

}
