<?php
namespace App\Http\Controllers;

use App\DetailPertemuan;
use App\Jawaban;
use Illuminate\Http\Request;
use App\Matakuliah;
use App\KelasFisik;
use App\KelasVirtual;
use App\Mahasiswa;
use App\AmbilKelas;
use App\Pertemuan;
use App\Materi;
use App\Dokumen;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Evaluasi;
use App\Soal;
use App\SoalEssay;
use App\Essay;

//use Input;
class DosenController extends Controller
{

  use AuthenticatesUsers;
    protected function guard()
    {
        return Auth::guard('dosen');
    }
    function authenticated(Request $request, $user)
    {
        session(['usertype' => 2]);

    }
  function dashboard()
  {

    $data["kelasVirtual"]=KelasVirtual::where("id_dosen",Auth::guard('dosen')->id())->get();

    return view("dosen.dashboard",$data);
  }
    public function showLoginForm()
    {
        return view('dosen.login');
    }

  protected function redirectTo(){
      return '/dosen/dashboard';
  }

  public function username()
   {
       return 'id_dosen';
   }

  function kelas($id)
  {
    $data["anggota"]=AmbilKelas::where("id_kelas_virtual","=",$id)->get();
    return view("dosen.anggota_kelas",$data);
  }

  function accept($id,$nim,$status){
    if ($status=='1') {
      AmbilKelas::where("id_kelas_virtual","=",$id)->where("nim","=",$nim)->update(["status" => "1"]);
    }elseif ($status=='2') {
      AmbilKelas::where("id_kelas_virtual","=",$id)->where("nim","=",$nim)->update(["status" => "2"]);
    }
    return redirect("dosen/kelas/".$id);
  }

  function pertemuan($id)
  {
    $data["pertemuan"]=Pertemuan::where("id_kelas_virtual","=",$id)->orderBy("urutan")->get();
    $data["id"]=$id;
    return view("dosen.pertemuan",$data);
  }
  function tambahPertemuan(Request $request)
  {
    $action=$request->action;
    $pertemuan=new Pertemuan;
    $pertemuan->id_pertemuan=$request->id_pertemuan;
    $pertemuan->id_kelas_virtual=$request->id_kelas_virtual;
    $pertemuan->nama=$request->nama;
    $last = Pertemuan::where("id_kelas_virtual",$request->id_kelas_virtual)->max("urutan");

    if($last){
        $pertemuan->urutan = $last + 1;
    } else {
        $pertemuan->urutan = 1;
    }

      if ($action=='0') {
      $pertemuan->save();
    }else {
      Pertemuan::where("id_pertemuan","=",$request->id_pertemuan)->update(["nama" => $pertemuan->nama]);
    }
    return redirect("dosen/pertemuan/".$request->id_kelas_virtual);
  }
  function hapusPertemuan($id,$idKelas)
  {
    Pertemuan::where("id_pertemuan","=",$id)->delete();
    $detail = DetailPertemuan::where("id_pertemuan",$id)->get();
    foreach($detail as $data){
        if($data->tipe == "materi"){
            $materi = Materi::find($data->id_konten);
            $destinationPath = public_path('upload/content_utama'); // upload path
            unlink($destinationPath."/".$materi->konten_utama);
            $materi->delete();
        } else if($data->tipe == "evaluasi") {
            Evaluasi::find($data->id_konten)->delete();
        } else {
            Essay::find($data->id_konten)->delete();
        }
        $data->delete();

    }

    return redirect("dosen/pertemuan/".$idKelas);
  }
  function materi($id)
  {
   // $data["materi"]=Materi::where("id_pertemuan",$id)->get();
    $data["pertemuan"]= Pertemuan::find($id);
    $data["detail"] = DetailPertemuan::where("id_pertemuan",$id)->orderBy("urutan")->get();
    return view('dosen.materi',$data);
  }
  function addMateri(Request $request)
  {
    $materi=new Materi;
    $materi->nama=$request->nama_materi;
    $materi->deskripsi=$request->deskripsi;

    if ($request->file('konten_utama')) {
      $destinationPath = public_path('upload/content_utama'); // upload path
      $extension = $request->file('konten_utama')->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      $request->file('konten_utama')->move($destinationPath, $fileName); // uploading file to given path
        $materi->konten_utama=$fileName;
    }

    $materi->save();


      $detail = new DetailPertemuan;
      $detail->id_pertemuan = $request->id_pertemuan;
      $detail->tipe = "materi";
      $last = DetailPertemuan::where("id_pertemuan",$request->id_pertemuan)->max("urutan");
      if($last){
          $detail->urutan = $last + 1;
      } else {
          $detail->urutan = 1;
      }

      $detail->id_konten = $materi->id;
      $detail->save();

    return redirect("dosen/materi/".$request->id_pertemuan);
  }
  function update_nama_materi(Request $request)
  {
    $materi = Materi::find($request->edit_id_materi_nama);
    $materi->nama = $request->edit_nama_materi;
    $materi->save();
    return redirect("dosen/materi/".$request->edit_id_pertemuan_nama);
  }
  function hapus_konten_utama(Request $request,$id_materi){
      $detail = DetailPertemuan::find($id_materi);
      $materi = Materi::find($detail->id_konten);
      $destinationPath = public_path('upload/content_utama'); // upload path
      unlink($destinationPath."/".$materi->konten_utama);
      $materi->konten_utama = "";
      $materi->save();
      return redirect("dosen/materi/".$detail->id_pertemuan);
  }
  function tambah_konten_utama(Request $request){
      $validator = Validator::make($request->all(), [

          'edit_konten_utama' => 'max:500000'
      ]);
      if ($validator->fails()) {
          return $validator->errors();
      } else {
          $destinationPath = public_path('upload/content_utama'); // upload path
          $extension = $request->file('edit_konten_utama')->getClientOriginalExtension(); // getting image extension
          $fileName = rand(11111,99999).'.'.$extension; // renameing image
          $request->file('edit_konten_utama')->move($destinationPath, $fileName); // uploading file to given path
          $materi = Materi::find($request->tambah_id_materi_konten_utama);

          $materi->konten_utama = $fileName;
          $materi->save();

          return redirect("dosen/materi/".$request->edit_id_pertemuan_konten_utama);
      }


  }
  function update_konten_utama(Request $request)
  {
    $validator = Validator::make($request->all(), [

       'edit_konten_utama' => 'max:500000'
      ]);
      if ($validator->fails()) {
        return $validator->errors();
      } else {
        $destinationPath = public_path('upload/content_utama'); // upload path
        $extension = $request->file('edit_konten_utama')->getClientOriginalExtension(); // getting image extension
        $fileName = $request->edit_konten_utama_lawas.".".$extension; // renameing image
        if (file_exists($destinationPath."/".$fileName)) {

          unlink($destinationPath."/".$fileName);
        }
        $request->file('edit_konten_utama')->move($destinationPath, $fileName); // uploading file to given path
        return redirect("dosen/materi/".$request->edit_id_pertemuan_konten_utama);
      }
  }
  function update_deskripsi_materi(Request $request)
  {
    $materi = Materi::find($request->edit_id_materi_deskripsi);
    $materi->deskripsi = $request->edit_deskripsi;
    $materi->save();
    return redirect("dosen/materi/".$request->edit_id_pertemuan_deskripsi);
  }
  function hapusMateri($id)
  {
    $detail = DetailPertemuan::find($id);

    $materi=Materi::find($detail->id_konten);

    $destinationPath=public_path('upload/content_utama');
    $id_pertemuan=$detail->id_pertemuan;
    if($materi->konten_utama){
        if (file_exists($destinationPath."/".$materi->konten_utama)) {
            unlink($destinationPath."/".$materi->konten_utama);
        }
    }
    $detail->delete();
    $materi->delete();

    return redirect("dosen/materi/".$id_pertemuan);
  }
  function deletePertemuan(Request $request,$id_pertemuan){

  }

  function uploadImage(Request $request){
      $path = $request->upload->store('images','public');
      $data['uploaded'] = 1;
      $data['fileName'] = basename($path);
      $data['url'] = asset("storage/".$path);
      return $data;
}



  function tambahMateri(Request $request,$id_pertemuan)
  {
      $data["pertemuan"] = Pertemuan::find($id_pertemuan);
      return view('dosen.tambah_materi', $data);
  }
  function tambahEvaluasi(Request $request,$id_pertemuan){
    $data['pertemuan'] = Pertemuan::find($id_pertemuan);
    return view('dosen.tambah_evaluasi',$data);
  }
  function postEvaluasi(Request $request,$id_pertemuan){
    $pertemuan = Pertemuan::find($id_pertemuan);

      $evaluasi = new Evaluasi;
      $evaluasi->nama = $request->nama;
      $evaluasi->status = 1;
      $evaluasi->save();
      $detail = new DetailPertemuan;
      $detail->id_pertemuan = $id_pertemuan;
      $detail->tipe = "evaluasi";
      $last = DetailPertemuan::where("id_pertemuan",$request->id_pertemuan)->max("urutan");
      if($last){
          $detail->urutan = $last + 1;
      } else {
          $detail->urutan = 1;
      }

      $detail->id_konten = $evaluasi->id;
      $detail->save();
      foreach($request->soal as $data){
        $soal = new Soal;
        $soal->id_evaluasi = $evaluasi->id;
        $soal->pertanyaan = $data['soal'];
        $soal->jawaban = $data['jawaban']['benar'];
        $soal->save();

        foreach($data['jawaban']['list'] as $key => $value){
          $jawaban = new Jawaban;
          $jawaban->index = $key;
          $jawaban->id_soal = $soal->id;
          $jawaban->jawaban = $value;
          $jawaban->save();
        }
      }
}
function getEvaluasi(Request $request,$id_evaluasi){
  $evaluasi = DetailPertemuan::find($id_evaluasi);
  $data = [];
  $data["nama"] = $evaluasi->konten->nama;
  $data["soal"] = [];
  foreach($evaluasi->konten->soal as $key => $val){
    $soal["id"] = $key;
    $soal["id_soal"] = $val->id;
    $soal["soal"] = $val->pertanyaan;
    $soal["jawaban"]["list"] = [];
    foreach($val->jawabanList as $val2){
      array_push($soal["jawaban"]["list"],$val2->jawaban);
    }
    $soal["jawaban"]["benar"] = $val->jawaban;
    array_push($data['soal'],$soal);
  }
  return $data;
}
function updateEvaluasi(Request $request,$id_evaluasi){
    $evaluasi = DetailPertemuan::find($id_evaluasi)->konten ;
    $evaluasi->nama = $request->nama;
    $evaluasi->save();

    Soal::where("id_evaluasi",$evaluasi->id)->delete();
    foreach($request->soal as $data){
        $soal = new Soal;
        $soal->id_evaluasi = $evaluasi->id;
        $soal->pertanyaan = $data['soal'];
        $soal->jawaban = $data['jawaban']['benar'];
        $soal->save();
        Jawaban::where("id_soal",$data["id_soal"])->delete();
        foreach($data['jawaban']['list'] as $key => $value){
            $jawaban = new Jawaban;
            $jawaban->index = $key;
            $jawaban->id_soal = $soal->id;
            $jawaban->jawaban = $value;
            $jawaban->save();
        }
    }

}
  function tambahDokumen(Request $request)
  {
    $mat=Materi::find($request->dokumen_id_materi);
    $dokumen=new Dokumen;
    $dokumen->id_dokumen="0";
    $dokumen->id_materi=$request->dokumen_id_materi;

    $validator = Validator::make($request->all(), [
       'edit_konten_utama' => 'max:500000'
      ]);
      if ($validator->fails()) {
        return $validator->errors();
      }else{
        $destinationPath = public_path('upload/dokumen'); // upload path
        $extension = $request->file('dokumen_nama')->getClientOriginalExtension(); // getting image extension
        $fileName = $mat->nama."-".date("Ymdhisa").".".$extension;
        $dokumen->nama=$fileName;
        $request->file('dokumen_nama')->move($destinationPath, $fileName); // uploading file to given path
        $dokumen->save();
        return redirect("dosen/materi/".$request->dokumen_id_pertemuan);
      }
  }
  function hapusDokumen($id,$idp)
  {
    $destinationPath=public_path('upload/dokumen');
    $dokumen=Dokumen::where("id_dokumen",$id)->first();
    if (file_exists($destinationPath."/".$dokumen->nama)) {
      unlink($destinationPath."/".$dokumen->nama);
    }
    Dokumen::where("id_dokumen",$id)->delete();
    return redirect("dosen/materi/".$idp);
  }
  function updateUrutan($detail_pertemuan,$tipe){

  }

  function simpanEssay(Request $request)
  {
    $ess=new Essay;
    $ess->nama=$request->nama;
    $ess->save();
    $detail = new DetailPertemuan;
    $detail->id_pertemuan = $request->id_pertemuan;
    $detail->tipe = "essay";
      $last = DetailPertemuan::where("id_pertemuan",$request->id_pertemuan)->max("urutan");
      if($last){
          $detail->urutan = $last + 1;
      } else {
          $detail->urutan = 1;
      }
    $detail->id_konten = $ess->id;
    $detail->save();
    //return redirect('dosen/tambahEssay/'.$ess->id);
    return redirect('dosen/materi/'.$detail->id_pertemuan);
  }
  function tambahEssay($id)
  {
    $data['id_essay']=$id;
    $ess=SoalEssay::where("id_essay",$id)->get();
    $data['count']=$ess->count();
    $data["data"]=$ess;
    return view('dosen/tambahEssay',$data);
  }
  function sortingMateri($id_detail,$direction){
    $detail = DetailPertemuan::find($id_detail);
    if($direction == "up"){
        $urutan = DetailPertemuan::where("id_pertemuan",$detail->id_pertemuan)->where("urutan",$detail->urutan - 1);
        if($urutan->count() > 0){
          $data = $urutan->first();
          $data->urutan = $detail->urutan;
          $detail->urutan = $detail->urutan - 1;
          $data->save();
          $detail->save();
        }
    } else {
        $urutan = DetailPertemuan::where("id_pertemuan",$detail->id_pertemuan)->where("urutan",$detail->urutan + 1);
        if($urutan->count() > 0){
            $data = $urutan->first();
            $data->urutan = $detail->urutan;
            $detail->urutan = $detail->urutan + 1;
            $data->save();
            $detail->save();
        }
    }
    return redirect("dosen/materi/".$detail->id_pertemuan);
  }

  function editDeskripsi($id_detail_pertemuan){
      $data['detail'] = DetailPertemuan::find($id_detail_pertemuan);
      return view("dosen.edit_deskripsi",$data);
  }
  function postDeskripsi(Request $request,$id_detail_pertemuan){
      $detail = DetailPertemuan::find($id_detail_pertemuan);
      $konten = Materi::find($detail->id_konten);
      $konten->deskripsi = $request->deskripsi;
      $konten->save();
      return redirect("dosen/materi/".$detail->id_pertemuan);
  }
  function deleteEvaluasi(Request $request,$id_detail){
      $detail = DetailPertemuan::find($id_detail);

      $evaluasi = Evaluasi::find($detail->id_konten);
      $soal = Soal::where("id_evaluasi",$evaluasi->id)->get();
      foreach($soal as $data){
          $jawaban = Jawaban::where("id_soal",$data->id);
          foreach($jawaban as  $value){
              $value->delete();
          }
          $data->delete();
      }
        $id_pertemuan = $detail->id_pertemuan;
      $evaluasi->delete();
      $detail->delete();
      return redirect("dosen/materi/".$id_pertemuan);
  }
    function sortingPertemuan($id_pertemuan,$direction){
        $pertemuan = Pertemuan::find($id_pertemuan);
        if($direction == "up"){
            $urutan = Pertemuan::where("id_kelas_virtual",$pertemuan->id_kelas_virtual)->where("urutan",$pertemuan->urutan - 1);
            if($urutan->count() > 0){
                $data = $urutan->first();
                $data->urutan = $pertemuan->urutan;
                $pertemuan->urutan = $pertemuan->urutan - 1;
                $data->save();
                $pertemuan->save();
            }
        } else {
            $urutan = Pertemuan::where("id_kelas_virtual",$pertemuan->id_kelas_virtual)->where("urutan",$pertemuan->urutan + 1);
            if($urutan->count() > 0){
                $data = $urutan->first();
                $data->urutan = $pertemuan->urutan;
                $pertemuan->urutan = $pertemuan->urutan + 1;
                $data->save();
                $pertemuan->save();
            }
        }
        return redirect("dosen/pertemuan/".$pertemuan->id_kelas_virtual);
    }
  function saveEssay(Request $request)
  {
    $status=$request->action;
    if ($status==0) {
      $soal=new SoalEssay;
    }else{
      $soal=SoalEssay::where("id",$request->id)->first();
    }
    $soal->id_essay=$request->id_essay;
    $soal->soal=$request->soal;
    $soal->tipe_jawaban=$request->tipe_jawaban;
    $soal->save();
    return redirect('dosen/tambahEssay/'.$request->id_essay);
  }
  function hapusSoalEssay($id)
  {
    $soal=SoalEssay::where("id",$id)->first();
    $id=$soal->id_essay;
    $soal->delete();
    return redirect('dosen/tambahEssay/'.$id);
  }
}
 ?>
