<?php

namespace App\Http\Middleware;

use Closure;

class MahasiswaAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('nim')) {
            return redirect('mahasiswa/login');
        }
        return $next($request);
    }
}
