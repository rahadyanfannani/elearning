<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KelasVirtual extends Model
{
  protected $table = "kelas_virtual";
  public $primaryKey = "id_kelas_virtual";

  function matakuliah(){ //one to one
       return $this->belongsTo('App\Matakuliah','kode_matkul','kode_matkul');
  }
  function dosen(){ //one to one
      return $this->belongsTo('App\Dosen',"id_dosen","id_dosen");
  }
  function kelas(){ //one to one
    return $this->belongsTo('App\KelasFisik',"id_kelas","id_kelas");
  }
  function ambilkelas(){
    return $this->belongsTo('App\AmbilKelas',"id_kelas_virtual","id_kelas_virtual");
  }
  function pertemuan(){
    return $this->haveMany('App\Pertemuan',"id_kelas_virtual","id_kelas_virtual");
  }
}
