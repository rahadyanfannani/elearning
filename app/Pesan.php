<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\PesanBroadcast;

class Pesan extends Model
{
    protected $table = "pesan";
    public function user()
    {
        if($this->status_user == 1) {// Mahasiswa
          return $this->belongsTo('App\Mahasiswa');
        } else {
          return $this->belongsTo('App\Dosen');
        }
    }
    public static function getPesanBroadcast($id_kelas){
       $data = Pesan::where('id_kelas',$id_kelas)->get();
       $list = [];
       foreach($data as $d){
         $new = new PesanBroadcast;
         $new->username = Mahasiswa::find(\Auth::id())->nama;
         $new->id_kelas = $id_kelas;
         $new->pesan = $d->pesan;
         array_push($list,$new);
       }
       return $list;
    }
}
