<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpsiJawaban extends Model
{
  protected $table = "opsi_jawaban";

  function rincianSoal(){
    return $this->belongsTo("App\RincianSoal","id_item","id_item");
  }
}
