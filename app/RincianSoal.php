<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RincianSoal extends Model
{
  protected $table = "rincian_soal";

  function opsi_jawaban(){
    return $this->haveMany("App\OpsiJawaban","id_item","id_item");
  }
  function essay(){
    return $this->haveMany("App\Essay","id_item","id_item");
  }
  function soal(){
    return $this->haveMany("App\Soal","id_soal","id_soal");
  }
}
