<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
  protected $table = "soal";

  function jawabanList(){
      return $this->hasMany('App\Jawaban',"id_soal","id");
  }

}
