<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Dosen  extends Authenticatable
{
    use Notifiable;

    protected $table = "dosen";
    protected $primaryKey = "id_dosen";
    public $incrementing = false;


    function kelasVirtual(){
      return $this->haveMany("App\KelasVirtual","id_dosen","id_dosen");
    }
}
