<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::get('/kelas/{id_kelas}',"ForumController@beranda");


//MAHASISWA AUTHENTICATION
Route::get('mahasiswa/login', 'MahasiswaController@showLoginForm')->name('mahasiswa.login');
Route::post('mahasiswa/login', 'MahasiswaController@login');

//DOSEN AUTHENTICATION
Route::get('dosen/login', 'DosenController@showLoginForm')->name('dosen.login');
Route::post('dosen/login', 'DosenController@login');

Route::get('admin/login',"AdminController@showLoginForm")->name('admin.login');
Route::post('admin/login',"AdminController@login");


Route::group(['middleware' => 'mahasiswa'], function () {
    Route::post('/mahasiswa/tambahKelas',"MahasiswaController@tambahKelas");
    Route::post('/kelas/{id_kelas}/create-message',"ForumController@createMessage");
    Route::get('/mahasiswa/getKelas/{kode_matkul}',"MahasiswaController@getKelas");
    Route::get('/kelas/{id_kelas}/get-message',"ForumController@getMessage");
    Route::get('/mahasiswa/dashboard',"MahasiswaController@dashboard");
    Route::get('mahasiswa/logout', 'MahasiswaController@logout');
    Route::get('/mahasiswa/materi/{id_kelas_virtual}/{id_pertemuan}',"MahasiswaController@materi");
    Route::get('/mahasiswa/list_pertemuan/{id_kelas_virtual}',"MahasiswaController@list_pertemuan");
    Route::get('/mahasiswa/evaluasi/{id_evaluasi}',"EvaluasiController@evaluasi");
    Route::get('/api/evaluasi/{id_evaluasi}',"EvaluasiController@getEvaluasi");
    Route::post('/api/evaluasi/{id_evaluasi}',"EvaluasiController@postEvaluasi");
    Route::post("/soal-essay/submit","EvaluasiController@postEssay");
});



Route::group(['middleware' => 'dosen'], function () {
    Route::get('/dosen/dashboard',"DosenController@dashboard");
  Route::get('/dosen/kelas/{id_kelas}',"DosenController@kelas");
  Route::get('/dosen/pertemuan/{id_kelas}',"DosenController@pertemuan");
  Route::get('/dosen/accept/{id_kelas}/{nim}/{status}',"DosenController@accept");
  Route::post('/dosen/tambahPertemuan',"DosenController@tambahPertemuan");
  Route::post('/dosen/addMateri',"DosenController@addMateri");
  Route::get('/dosen/hapusPertemuan/{id_pertemuan}/{id_kelas_virtual}',"DosenController@hapusPertemuan");
  Route::get('/dosen/materi/{id_pertemuan}',"DosenController@materi");
  Route::get('/dosen/hapusMateri/{id_materi}',"DosenController@hapusMateri");

  Route::get('/dosen/hapusDokumen/{id_dokumen}/{id_pertemuan}',"DosenController@hapusDokumen");
  Route::post('/dosen/tambahDokumen',"DosenController@tambahDokumen");
  Route::get('/dosen/tambahMateri/{id_pertemuan}',"DosenController@tambahMateri");
  Route::get('dosen/logout', 'DosenController@logout');
  Route::post('/dosen/update_nama_materi',"DosenController@update_nama_materi");
  Route::post('/dosen/update_deskripsi_materi',"DosenController@update_deskripsi_materi");
  Route::post('/dosen/update_konten_utama',"DosenController@update_konten_utama");
  Route::post('/dosen/tambah_konten_utama',"DosenController@tambah_konten_utama");
  Route::get('/dosen/hapus-file/{id_materi}',"DosenController@hapus_konten_utama");


  Route::get('/dosen/tambah-evaluasi/{id_pertemuan}','DosenController@tambahEvaluasi');
  Route::get('/dosen/evaluasi/{id_evaluasi}','DosenController@getEvaluasi');
  Route::post('/dosen/tambah-evaluasi/{id_pertemuan}','DosenController@postEvaluasi');
  Route::post('/dosen/edit-evaluasi/{id_evaluasi}','DosenController@updateEvaluasi');

  Route::get('/dosen/tambahEssay/{id_essay}','DosenController@tambahEssay');
  Route::get('/dosen/hapusSoalEssay/{id}','DosenController@hapusSoalEssay');
  Route::post('/dosen/simpanEssay','DosenController@simpanEssay');
  Route::post('/dosen/saveEssay','DosenController@saveEssay');
  Route::get("/dosen/sort/{id_detail}/{direction}","DosenController@sortingMateri");
  Route::get("/dosen/pertemuan/sort/{id_pertemuan}/{direction}","DosenController@sortingPertemuan");
  Route::post("/dosen/upload-image","DosenController@uploadImage");
  Route::get("/dosen/edit-deskripsi/{id_detail_pertemuan}","DosenController@editDeskripsi");
  Route::post("/dosen/edit-deskripsi/{id_detail_pertemuan}","DosenController@postDeskripsi");
  Route::get("dosen/delete-evaluasi/{id_detail_pertemuan}","DosenController@deleteEvaluasi");

});



Auth::routes();

Route::get('/home', 'HomeController@home');

Route::get('/admin/matakuliah',"Admin\MatakuliahController@home");

Route::get('/admin/dosen',"Admin\DosenController@home");

Route::get('/admin/dosen/tambah',"Admin\DosenController@tambah");

Route::post('/admin/dosen/tambah',"Admin\DosenController@postTambah");

Route::get('/admin/dosen/edit/{id_dosen}',"Admin\DosenController@edit");
Route::post('/admin/dosen/edit/{id_dosen}',"Admin\DosenController@postedit");
Route::get('/admin/dosen/hapus/{id_dosen}',"Admin\DosenController@hapus");
Route::get('/admin/matakuliah/tambah',"Admin\MatakuliahController@tambah");
Route::post('/admin/matakuliah/tambah',"Admin\MatakuliahController@postTambah");
Route::get('/admin/matakuliah/edit/{kode_matkul}',"Admin\MatakuliahController@edit");
Route::post('/admin/matakuliah/edit/{kode_matkul}',"Admin\MatakuliahController@postedit");
Route::get('/admin/matakuliah/hapus/{kode_matkul}',"Admin\MatakuliahController@hapus");


Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@home');
});

Route::get("/activate","HomeController@activate");
Route::get("/unggah","HomeController@upload");
Route::post("/unggah","HomeController@postUpload");

Route::get("/admin/home","AdminController@home");
Route::get("/admin/kelas","AdminController@kelas");
Route::get("/admin/kelas_virtual","AdminController@kelas_virtual");
Route::get("/admin/matakuliah","AdminController@matakuliah");
Route::get("/admin/delete_kelas/{id_kelas}","AdminController@delete_kelas");
Route::post("/admin/save_kelas","AdminController@save_kelas");
Route::get("/admin/delete_kelas_virtual/{id_kelas_virtual}","AdminController@delete_kelas_virtual");
Route::post("/admin/save_kelas_virtual","AdminController@save_kelas_virtual");
Route::get("/admin/delete_matakuliah/{kode_matkul}","AdminController@delete_matakuliah");
Route::post("/admin/save_matakuliah","AdminController@save_matakuliah");
Route::get("/admin/dosen","AdminController@dosen");
Route::get("/admin/delete_dosen/{id_dosen}","AdminController@delete_dosen");
Route::post("/admin/save_dosen","AdminController@save_dosen");
