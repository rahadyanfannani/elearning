
@extends('layouts.appv2')
@section('content')
  <!-- Page title -->
  <div class="page-title parallax parallax1">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title-heading">
            <h1 class="title">{{ $pertemuan->nama." (".$pertemuan->kelasVirtual->matakuliah->nama_matkul.")" }} </h1>
            <div class="breadcrumbs">
              <ul>
                <li><a href="{{ url('mahasiswa/list_pertemuan/'.$pertemuan->id_kelas_virtual)}}">Kembali ke list</a></li>

              </ul>
            </div>
          </div><!-- /.page-title-captions -->

        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /.page-title -->

  <section class="flat-row">
    <div class="course-single">
      <div class="container">

        <div class="row">
          <div class="col-md-4">
            <div class="course-sidebar">

              <div class="course-widget-categories">

                <h6 class="course-widget-title">Daftar Materi</h6>
                <ul>
                    <?php  foreach ($detail as $key => $value) {?>
                  @if($value->tipe == "evaluasi")
                    <li><a href="#evaluasi{{ $value->konten->id }}" data-toggle="pill">  <span> {{ $value->konten->nama }} </span> </a></li>
                    @elseif($value->tipe == "essay")
                        <li><a href="#essay{{ $value->konten->id }}" data-toggle="pill">  <span> {{ $value->konten->nama }} </span> </a></li>

                      @else
                    <li><a href="#{{ $value->konten->id }}" data-toggle="pill">  <span> {{ $value->konten->nama }} </span> </a></li>
                  @endif
                    <?php }?>

                </ul>
              </div><!-- /.widget -->
          </div>

        </div>
          <div class="col-md-8">
            <div class="tabs-vertical-env">
              <div class="tab-content col-md-12">

                  <?php foreach ($pertemuan->detail as $key => $value) {?>
                @if($value->tipe == "materi")
                  <div class="tab-pane" id="{{ $value->konten->id }}">

                    <div class="course-entry">
                      <h6 class="course-title">{{ $value->konten->nama }}</h6>

                      <div class="course-image">
                          <?php if (file_exists(public_path('upload/content_utama/'.$value->konten->konten_utama)) && $value->konten->konten_utama != "") {?>
                        <video class="col-sm-12" controls>
                          <source src="{{ asset('upload/content_utama/'.$value->konten->konten_utama) }}" type="video/webm">
                          Your browser does not support the video tag.
                        </video>
                          <?php } ?>
                      </div>
                      <h6 class="title"></h6>

                      <div class="content-desc">
                        {!! $value->konten->deskripsi !!}
                      </div>




                          <table class="table m-0 table-colored-full table-hover">
                            <thead>
                            <tr>
                              <th><b>#</b></th>
                              <th><b>Nama Dokumen</b></th>
                              <th><b>Opsi</b><th>
                            </tr>
                            </thead>
                              <?php
                              $i=1;
                              foreach ($value->konten->dokumen as $ky => $val) {?>
                            <tr>
                              <td>{{ $i }}</td>
                              <td>{{ $val->nama }}</td>
                              <td>
                                <a href="{{ asset('upload/dokumen/'.$val->nama) }}">
                                  <button type="button" name="button" class="btn btn-success">
                                    <i class="fa fa-download m-r-5"> </i> <span>
                                Download File </span>
                                  </button>
                                </a>
                              </td>
                            </tr>
                              <?php $i++; }
                              ?>
                          </table>


                    </div>
                  </div>
                @elseif($value->tipe == "evaluasi")
                  <div class="tab-pane" id="evaluasi{{ $value->konten->id }}">
                    <div class="course-entry">
                      <h6 class="course-title">{{ $value->konten->nama }}</h6>

                    <div id="app">

                      <evaluasi post-url="{{ url('/api/evaluasi/'.$value->id) }}" get-url="{{ url('/api/evaluasi/'.$value->id) }}  "></evaluasi>

                    </div>
                  </div>
                  </div>
                    @elseif($value->tipe == "essay")
                      <div class="tab-pane" id="essay{{ $value->konten->id }}">
                        <div class="course-entry">
                          <h6 class="course-title">{{ $value->konten->nama }}</h6>


                            <?php $i = 1 ?>
                            @foreach($value->konten->soal as $data)
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="panel panel-default">
                                    <div class="panel-heading">No. {{ $i  }}</div>
                                                          <?php $i++ ?>
                                    <div class="panel-body" >
                                      <div class="soal">
                              {!!  $data->soal   !!}
                                      </div>
                                      <textarea style="margin-top:10px" class="type-input" tabindex="3" placeholder="Jawaban anda..." name="message" id="message-contact" required=""></textarea>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              @endforeach
                              <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                            <button>Kirim</button>
                                </div>
                              </div>


                      </div>
                      </div>
                @endif
                  <?php } ?>

              </div><!-- tab content -->

            </div>

          </div><!-- end of container -->
        </div>
      </div>
    </div>
  </section>
@endsection
