@extends('layouts.app_materi')
@section('navigation')
    <li class="home">
        <a href="{{ url('mahasiswa/dashboard')  }}">Dashboard </a>

    </li>
    <li>    <a href="{{ url('kelas/'.$kelas->id_kelas_virtual) }}">Forum</a>
    </li>

@endsection
@section('content')
    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Selamat Datang di Kelas {{ $kelas->matakuliah->nama_matkul }} </h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="index.html">Dashboard</a></li>
                                <li>Kelas</li>
                            </ul>
                        </div>
                    </div><!-- /.page-title-captions -->

                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->


    <section class="flat-row pad-top50px">
        <div class="container">
                <?php $i = 0;?>

                <?php foreach ($pertemuan as $key => $value) {?>
                        @if($i == $jumlah  )

                            </div>
                        @elseif($i % 4 == 0 && $i != 0 )

                                </div>
                                <div class="row">
                        @elseif($i == 0)

                            <div class="row">
                        @endif
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="flat-courses style1">
                            <div class="courses-thumbnail">
                                <a href="{{ url('mahasiswa/materi/'.$value->id_kelas_virtual.'/'.$value->id_pertemuan) }}"><img src="{{asset("assets/images/portfolio1.jpg")}}" alt="image"></a>

                            </div>
                            <div class="courses-content">
                                <a href="{{ url('mahasiswa/materi/'.$value->id_kelas_virtual.'/'.$value->id_pertemuan) }}"><h6 class="courses-topic">{{ $value->nama }}</h6></a>
                            </div>


                        </div><!-- /.flat-courses -->
                    </div><!-- /.col-md-3 -->

                        <?php
                        $i++;

                        ?>
                <?php } ?>



        </><!-- /.container -->

        <div class="flat-divider d40px"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-pagination style1">
                        <ul class="flat-pagination clearfix">
                            <li class="prev">
                                <a href="#"><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li class="active">1</li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li class="next">
                                <a href="#"><i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div><!-- /.blog-pagination -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-row -->

    <section class="flat-row pad-top50px pad-bottom0px">
<div class="container">
<div class="row">




</div><!-- end of container -->
</div>
    </section>


@endsection