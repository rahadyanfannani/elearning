
@extends('layouts.appv2')

@section('content')
    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Selamat Datang di Halaman Dashboard Mahasiswa</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="index.html">Dashboard</a></li>
                                <li>Ambil Matakuliah</li>
                            </ul>
                        </div>
                    </div><!-- /.page-title-captions -->

                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->


    <!-- Fillter courses -->
    <section class="flat-row pad-top50px pad-bottom0px">
        <div class="container">
            <div class="row">
                <div class="search-course">
                    <div class="col-md-10">
                        <!-- Fillter courses -->


                        <form class="flat-contact-form fillter-courses border-radius border-white text-center style1" id="contactform5"  action="{{ url('mahasiswa/tambahKelas') }}" method="post">
                            {{ csrf_field() }}
                            <div class="field clearfix">
                                <div class="wrap-type-input">
                                    <div class="wrap categories-courses"> <p> Pilih Matakuliah yang tersedia
                                        <select class="select-field categories-courses"  name="matakuliah" onchange="FindKelas(this.value)">
                                            <option value="">Matakuliah</option>
                                            <?php foreach ($matakuliah as $key => $value){ ?>

                                            <option value="<?php echo $value->kode_matkul;?>" class="btn btn-default btn-rounded w-md waves-effect"><?php echo $value->nama_matkul;?></option>
                                            <?php } ?>
                                        </select> </p>

                                    </div><!-- /.wrap-select -->
                                    <div id="kelas">

                                    </div>
                                </div><!-- /.wrap-type-input -->
                            </div><!-- /.field -->
                        </form><!-- /.comment-form -->
                    </div><!-- /.col-md-10 -->


                </div><!-- /.search-course -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>



    <!--<a href="{{ url('mahasiswa/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>-->
    <section class="flat-row pad-top50px pad-bottom0px">
        <div class="container">
            <div class="row">
        <h3 class="box-title"><b>Data Ambil Kelas</b></h3>

        <table class="table m-0 table-colored-full table-hover">
          <thead>
		  <tr>
            <th><b>Matakuliah</b></th>
            <th><b>Kelas</b></th>
            <th><b>Status</b></th>
          </tr>
		  </thead>
          <?php foreach ($ambil_kelas as $value) {?>

            <tr>
              <td>{{ $value->kelasVirtual->matakuliah->nama_matkul }}</td>
              <td>{{ $value->kelasVirtual->kelas->nama_kelas }}</td>
              <td>
                <?php
                if ($value->status=="0") {?>
                  <span class="label label-warning">
                    Belum dikonfirmasi Dosen
                  </span>
                <?php }else if($value->status=='1'){?>
                  <a href="{{ url('kelas/'.$value->id_kelas_virtual) }}"><button type="button" name="button" class="btn btn-success btn-sm">Masuk</button></a>
                <?php }else{?>
                  <span class="label label-danger">
                    Ditolak Dosen
                  </span>
                <?php }
                 ?>
              </td>
            </tr>
          <?php } ?>
        </table>
            </div></div>
    </section>

<script type="text/javascript">
  var ajaxku;
  function buatajax(){
  	if (window.XMLHttpRequest){
  		return new XMLHttpRequest();
  	}
  	if (window.ActiveXObject){
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	return null;
  }
  function InHTML(id){
  	var data;
  	if (ajaxku.readyState==4){
  		data=ajaxku.responseText;
  		if(data.length>0){
  			document.getElementById(id).innerHTML = data;
  		}else{
  			document.getElementById(id).innerHTML = "Data Tidak Ada!";
  		}
  	}
  }
  function FindKelas(matkul)
  {
    ajaxku = buatajax();
	  ajaxku.onreadystatechange=function(){InHTML('kelas')};
	  ajaxku.open("GET",'{{ url("mahasiswa/getKelas") }}'+"/"+matkul,true);
	  //ajaxku.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  ajaxku.send(null);
  }
</script>
@endsection

