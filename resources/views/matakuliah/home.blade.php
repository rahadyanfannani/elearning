<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title> Matakuliah </title>
  </head>
  <body>
    <a href="{{ url('admin/matakuliah/tambah') }}">Tambah Matakuliah</a>
    <table>
      <tr>
        <th>Kode Matkul</th>
        <th>Nama Matkul</th>
      </tr>
      @foreach($matkul as $data)
      <tr>
        <td>{{ $data->kode_matkul }}</td>
        <td>{{ $data->nama_matkul }}</td>
        <td><a href="{{ url('admin/matakuliah/edit/'.$data->kode_matkul) }}">EDIT</a></td>
        <td><a href="{{ url('admin/matakuliah/hapus/'.$data->kode_matkul) }}">HAPUS</a></td>
      </tr>
      @endforeach
    </table>
  </body>
</html>
