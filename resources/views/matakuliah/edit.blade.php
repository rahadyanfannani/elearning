<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Matakuliah</title>
  </head>
  <body>
    <form  action="{{ url('admin/matakuliah/edit/'.$matakuliah->kode_matkul) }}" method="post">
      {{ csrf_field() }}
      <table>
        <tr>
          <td>Kode Matkul</td>
          <td>:</td>
          <td><input type="text" name="kode_matkul" value="{{$matakuliah->kode_matkul}}"></td>
        </tr>
        <tr>
          <td>Nama Matakuliah</td>
          <td>:</td>
          <td><input type="text" name="nama_matkul" value="{{$matakuliah->nama_matkul}}"></td>
        </tr>
        <tr>
      </table>
      <input type="SUBMIT"  value="Simpan">


    </form>

  </body>
</html>
