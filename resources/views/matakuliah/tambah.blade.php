<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tambah Matakuliah</title>
  </head>
  <body>
    <form  action="{{ url('admin/matakuliah/tambah') }}" method="post">
      {{ csrf_field() }}
      <table>
        <tr>
          <td>Kode Matakuliah </td>
          <td>:</td>
          <td><input type="text" name="kode_matkul" ></td>
        </tr>
        <tr>
          <td>Nama Matakuliah</td>
          <td>:</td>
          <td><input type="text" name="nama_matkul"></td>
        </tr>
      </table>
      <input type="SUBMIT"  value="Simpan">


    </form>

  </body>
</html>
