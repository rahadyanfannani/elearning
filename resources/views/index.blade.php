<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ELearning - Teknik Elektro UM</title>
	<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">

	<!-- JS -->
    <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

@extends('layouts.appv1')
@section('content')
  <body class="bg-home">
    <!-- <img src="{{ asset('assets/images/slide1.png') }}" style="background-size: cover;"> -->
    <div class="container-alt">
    <!--<div class="row-m-t-50 mrg-top">
        <div class="col-sm-12">
            <img src="{{ asset('assets/images/patch page.png') }}">
        </div>
    </div>-->

	  <div class="row-m-t-50 mrg-top">
		<div class="col-sm-12">
            <center>

            <img src="{{ asset("assets/images/logo_elearning.png") }}" width="200" alt=""><br>
            <img src="{{ asset("assets/images/patch page.png") }}" width="800" alt="" style="margin-top:90px">
            </center>
            <div class="wrapper-page">
		  <div class="col-sm-5">
			<a href="{{ url('/mahasiswa/login') }}"> <img src="{{ asset('assets/images/btn_mahasiswa.png') }}" width="180px"></a>
		  </div>
		  <div class="col-sm-5">
			<a href="{{ url('/dosen/login') }}"> <img src="{{ asset('assets/images/btn_dosen.png') }}" width="180px"> </a>
		  </div>
		  </div>
		</div>
	  </div>
	</div>
  </body>
@endsection
</html>
