@extends('layouts.admin')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">
      Daftar Matakuliah Jurusan Teknik Elektro
    </h3>
    <div class="box-body">
      <table class="table table-bordered" id="table_matakuliah">
        <tr>
          <th>#</th>
          <th>Kode</th>
          <th>Nama Matakuliah</th>
          <th>Option</th>
        </tr>
        <?php $i=1;
        foreach ($matakuliah as $key => $value) {?>
          <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->kode_matkul }}</td>
            <td>{{ $value->nama_matkul }}</td>
            <td>
              <button type="button" name="button" class="btn btn-primary btn-sm" onclick="EditMatkul('{{ $i }}')">
                EDIT
              </button>
              <a href="{{ url('admin/delete_matakuliah/'.$value->kode_matkul) }}">
              <button type="button" name="button" class="btn btn-danger btn-sm">
                HAPUS
              </button>
              </a>
            </td>
          </tr>
        <?php $i++; } ?>
      </table>
    </div>
    <div class="box-footer">
      <button type="button" name="button" class="btn btn-primary pull-right" onclick="TambahMatkul()">
        Tambah
      </button>
    </div>
  </div>
</div>

<div class="modal modal-primary" id="modal_matakuliah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Form Kelas</h4>
      </div>
      <form class="form-horizontal" action="{{ url('admin/save_matakuliah') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="action" id="action" value="0">
        <div class="modal-body">
          <div class="form-group">
            <label class="contol-label col-sm-3">Kode Matakuliah</label>
            <div class="col-sm-9">
              <input type="text" name="kode_matkul" id="kode_matkul" placeholder="Kode Matakuliah" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="contol-label col-sm-3">Nama Matakuliah</label>
            <div class="col-sm-9">
              <input type="text" name="nama_matkul" id="nama_matkul" placeholder="Nama Matakuliah" required="required" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
          <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">

  function TambahMatkul()
  {
    var modal=document.getElementById("modal_matakuliah");
    document.getElementById('action').value="0";
    document.getElementById('kode_matkul').value="";
    document.getElementById('nama_matkul').value="";
    document.getElementById('kode_matkul').readOnly=false;
    modal.style.display='block';
  }
  function EditMatkul(row)
  {
    var modal=document.getElementById("modal_matakuliah");
    document.getElementById('action').value="1";
    document.getElementById('kode_matkul').value=document.getElementById('table_matakuliah').rows[row].cells[1].innerHTML;
    document.getElementById('kode_matkul').readOnly=true;
    document.getElementById('nama_matkul').value=document.getElementById('table_matakuliah').rows[row].cells[2].innerHTML;
    modal.style.display='block';
  }
  function CloseModal(){
    var modal=document.getElementById("modal_matakuliah");
    modal.style.display='none';
  }
  window.onclick=function(event){
    var modal=document.getElementById("modal_matakuliah");
    if(event.target==modal)  {
      modal.style.display='none';
    }
  }
</script>
