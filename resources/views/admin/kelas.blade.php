@extends('layouts.admin')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">
      Daftar Kelas Jurusan Teknik Elektro
    </h3>
    <div class="box-body">
      <table class="table table-bordered" id="table_kelas">
        <tr>
          <th>#</th>
          <th>ID Kelas</th>
          <th>Nama Kelas</th>
          <th>Option</th>
        </tr>
        <?php $i=1;
        foreach ($KelasFisik as $key => $value) {?>
          <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->id_kelas }}</td>
            <td>{{ $value->nama_kelas }}</td>
            <td>
              <button type="button" name="button" class="btn btn-primary btn-sm" onclick="EditKelas('{{ $i }}')">
                EDIT
              </button>
              <a href="{{ url('admin/delete_kelas/'.$value->id_kelas) }}">
              <button type="button" name="button" class="btn btn-danger btn-sm">
                HAPUS
              </button>
              </a>
            </td>
          </tr>
        <?php $i++; } ?>
      </table>
    </div>
    <div class="box-footer">
      <button type="button" name="button" class="btn btn-primary pull-right" onclick="TambahKelas()">
        Tambah
      </button>
    </div>
  </div>
</div>

<div class="modal modal-primary" id="modal_kelas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Form Kelas</h4>
      </div>
      <form class="form-horizontal" action="{{ url('admin/save_kelas') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id_kelas" id="id_kelas" value="">
        <input type="hidden" name="action" id="action" value="0">
        <div class="modal-body">
          <div class="form-group">
            <label class="contol-label col-sm-3">Nama Kelas</label>
            <div class="col-sm-9">
              <input type="text" name="nama_kelas" id="nama_kelas" placeholder="Nama Kelas" required="required" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
          <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">

  function TambahKelas()
  {
    var modal=document.getElementById("modal_kelas");
    document.getElementById('action').value="0";
    document.getElementById('id_kelas').value="0";
    document.getElementById('nama_kelas').value="";
    modal.style.display='block';
  }
  function EditKelas(row)
  {
    var modal=document.getElementById("modal_kelas");
    document.getElementById('action').value="1";
    document.getElementById('id_kelas').value=document.getElementById('table_kelas').rows[row].cells[1].innerHTML;
    document.getElementById('nama_kelas').value=document.getElementById('table_kelas').rows[row].cells[2].innerHTML;
    modal.style.display='block';
  }
  function CloseModal(){
    var modal=document.getElementById("modal_kelas");
    modal.style.display='none';
  }
  window.onclick=function(event){
    var modal=document.getElementById("modal_kelas");
    if(event.target==modal)  {
      modal.style.display='none';
    }
  }
</script>
