@extends('layouts.admin')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">
      Daftar Kelas Virtual Jurusan Teknik Elektro
    </h3>
    <div class="box-body">
      <table class="table table-bordered" id="table_kelas_virtual">
        <tr>
          <th>#</th>
          <th>Matakuliah</th>
          <th>Dosen</th>
          <th>Kelas</th>
          <th>Option</th>
        </tr>
        <?php $i=1;
        foreach ($KelasVirtual as $key => $value) {?>
          <tr>
            <td>{{ $i }}</td>
            <td>{{ $value->matakuliah->nama_matkul }}</td>
            <td>{{ $value->dosen->nama }}</td>
            <td>{{ $value->kelas->nama_kelas }}</td>
            <td>
              <button type="button" name="button" class="btn btn-primary btn-sm" onclick="EditKelas('{{ $value->id_kelas_virtual }}','{{ $value->kode_matkul }}','{{ $value->id_dosen }}','{{ $value->id_kelas }}')">
                EDIT
              </button>
              <a href="{{ url('admin/delete_kelas_virtual/'.$value->id_kelas_virtual) }}">
              <button type="button" name="button" class="btn btn-danger btn-sm">
                HAPUS
              </button>
              </a>
            </td>
          </tr>
        <?php $i++; } ?>
      </table>
    </div>
    <div class="box-footer">
      <button type="button" name="button" class="btn btn-primary pull-right" onclick="TambahKelas()">
        Tambah
      </button>
    </div>
  </div>
</div>

<div class="modal modal-primary" id="modal_kelas_virtual">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Form Kelas Virtual</h4>
      </div>
      <form class="form-horizontal" action="{{ url('admin/save_kelas_virtual') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="id_kelas_virtual" id="id_kelas_virtual" value="">
        <input type="hidden" name="action" id="action" value="0">
        <div class="modal-body">
          <div class="form-group">
            <label class="contol-label col-sm-3">Matakuliah</label>
            <div class="col-sm-9">
              <select class="form-control" name="kode_matkul" id="kode_matkul" required="required">
                <?php foreach ($matakuliah as $key => $value) {?>
                  <option value="{{ $value->kode_matkul }}">{{ $value->nama_matkul }}</option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="contol-label col-sm-3">Dosen</label>
            <div class="col-sm-9">
              <select class="form-control" name="id_dosen" id="id_dosen" required="required">
                <?php foreach ($dosen as $key => $value) {?>
                  <option value="{{ $value->id_dosen }}">{{ $value->nama }}</option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="contol-label col-sm-3">Kelas</label>
            <div class="col-sm-9">
              <select class="form-control" name="id_kelas" id="id_kelas" required="required">
                <?php foreach ($KelasFisik as $key => $value) {?>
                  <option value="{{ $value->id_kelas }}">{{ $value->nama_kelas }}</option>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
          <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">
  function cari(id,kode){
    var select=document.getElementById(id);
    for (var i = 0; i < select.options.length; i++) {
      if(select.options[i].value==kode){
        return i;
      }
    }
  }

  function TambahKelas()
  {
    var modal=document.getElementById("modal_kelas_virtual");
    document.getElementById('action').value="0";
    document.getElementById('id_kelas_virtual').value="0";
    document.getElementById('id_kelas').selectedIndex=0;
    document.getElementById('id_dosen').selectedIndex=0;
    document.getElementById('kode_matkul').selectedIndex=0;
    modal.style.display='block';
  }
  function EditKelas(id,kode_matkul,id_dosen,id_kelas)
  {
    var modal=document.getElementById("modal_kelas_virtual");
    document.getElementById('action').value="1";
    document.getElementById('id_kelas_virtual').value=id;
    document.getElementById('kode_matkul').selectedIndex=cari('kode_matkul',kode_matkul);
    document.getElementById('id_dosen').selectedIndex=cari('id_dosen',id_dosen);
    document.getElementById('id_kelas').selectedIndex=cari('id_kelas',id_kelas);
    modal.style.display='block';
  }
  function CloseModal(){
    var modal=document.getElementById("modal_kelas_virtual");
    modal.style.display='none';
  }
  window.onclick=function(event){
    var modal=document.getElementById("modal_kelas_virtual");
    if(event.target==modal)  {
      modal.style.display='none';
    }
  }
</script>
