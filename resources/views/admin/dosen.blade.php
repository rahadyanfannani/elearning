@extends('layouts.admin')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">
      Daftar Dosen Jurusan Teknik Elektro
    </h3>
    <div class="box-body">
      <table class="table table-bordered" id="table_dosen">
        <tr>
          <th>ID Dosen</th>
          <th>Nama Dosen</th>
          <th>Email</th>
          <th>Alamat</th>
          <th>No. Telp</th>
          <th>Option</th>
        </tr>
        <?php $i=1;
        foreach ($dosen as $key => $value) {?>
          <tr>
            <td>{{ $value->id_dosen }}</td>
            <td>{{ $value->nama }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->alamat }}</td>
            <td>{{ $value->no_telp }}</td>
            <td>
              <button type="button" name="button" class="btn btn-primary btn-sm" onclick="EditDosen('{{ $i }}','{{ $value->password }}')">
                EDIT
              </button>
              <a href="{{ url('admin/delete_dosen/'.$value->id_dosen) }}">
              <button type="button" name="button" class="btn btn-danger btn-sm">
                HAPUS
              </button>
              </a>
            </td>
          </tr>
        <?php $i++; } ?>
      </table>
    </div>
    <div class="box-footer">
      <button type="button" name="button" class="btn btn-primary pull-right" onclick="TambahDosen()">
        Tambah
      </button>
    </div>
  </div>
</div>

<div class="modal modal-primary" id="modal_dosen">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Form Dosen</h4>
      </div>
      <form class="form-horizontal" action="{{ url('admin/save_dosen') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="action" id="action" value="0">
        <div class="modal-body">
          <div class="form-group">
            <label class="contol-label col-sm-3">ID Dosen</label>
            <div class="col-sm-9">
              <input type="text" name="id_dosen" id="id_dosen" placeholder="ID Dosen" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="contol-label col-sm-3">Nama</label>
            <div class="col-sm-9">
              <input type="text" name="nama" id="nama" placeholder="Nama Dosen" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="contol-label col-sm-3">Email</label>
            <div class="col-sm-9">
              <input type="text" name="email" id="email" placeholder="Email Dosen" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="contol-label col-sm-3">Alamat</label>
            <div class="col-sm-9">
              <input type="text" name="alamat" id="alamat" placeholder="Alamat Dosen" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="contol-label col-sm-3">No. telp</label>
            <div class="col-sm-9">
              <input type="text" name="no_telp" id="no_telp" placeholder="Nomor telepon Dosen" required="required" class="form-control">
            </div>
          </div>
          <div class="form-group" id="form_password">
            <label class="contol-label col-sm-3">Password</label>
            <div class="col-sm-9">
              <input type="password" name="password" id="password" placeholder="Password Dosen" required="required" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
          <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

<script type="text/javascript">

  function TambahDosen()
  {
    var modal=document.getElementById("modal_dosen");
    document.getElementById('action').value="0";
    document.getElementById('id_dosen').value="";
    document.getElementById('nama').value="";
    document.getElementById('email').value="";
    document.getElementById('alamat').value="";
    document.getElementById('no_telp').value="";
    document.getElementById('id_dosen').readOnly=false;
    document.getElementById('form_password').style.visibility="visible";
    modal.style.display='block';
  }
  function EditDosen(row,pass)
  {
    var modal=document.getElementById("modal_dosen");
    document.getElementById('action').value="1";

    document.getElementById('id_dosen').value=document.getElementById('table_dosen').rows[row].cells[0].innerHTML;
    document.getElementById('nama').value=document.getElementById('table_dosen').rows[row].cells[1].innerHTML;
    document.getElementById('email').value=document.getElementById('table_dosen').rows[row].cells[2].innerHTML;
    document.getElementById('alamat').value=document.getElementById('table_dosen').rows[row].cells[3].innerHTML;
    document.getElementById('no_telp').value=document.getElementById('table_dosen').rows[row].cells[4].innerHTML;
    document.getElementById('password').value=pass;
    document.getElementById('form_password').style.visibility="hidden";
    document.getElementById('id_dosen').readOnly=true;
    modal.style.display='block';
  }
  function CloseModal(){
    var modal=document.getElementById("modal_dosen");
    modal.style.display='none';
  }
  window.onclick=function(event){
    var modal=document.getElementById("modal_dosen");
    if(event.target==modal)  {
      modal.style.display='none';
    }
  }
</script>
