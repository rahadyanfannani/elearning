<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login Dosen - ELearning - Teknik Elektro UM</title>

  <!-- App CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">

  <!-- JS -->
  <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>
</head>
@extends('layouts.appv1')
@section('content')
<body class="bg-login">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<section>
<div class="container-alt">
	<div class="row mrg-top2">
            <div class="col-sm-12">

                <div class="wrapper-page">
                    <div class="m-t-40 account-pages">
                        <div class="text-center">
                            <h2 class="text-uppercase">
                                <font color="#ffffff"> Form Login </font>
                            </h2>
                            <!--<h4 class="text-uppercase font-bold m-b-0">Sign In</h4>-->
	                    </div>
                                <div>
                                    <form class="form-horizontal" action="{{ url('admin/login') }}" method="post">
									{{ csrf_field() }}
                                        <div class="form-group ">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="text" name="email" placeholder="Email" required/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <input class="form-control" type="password" name="password" placeholder="Password" required/>
                                            </div>
                                        </div>

                                        <div class="form-group account-btn text-center m-t-10">
                                            <div class="col-xs-12">
                                                <button class="btn btn-primary waves-effect w-md waves-light" type="submit">Log In</button>
                                            </div>
                                        </div>

                                    </form>
                                    <div class="clearfix"></div>
                            </div>
                            <!-- end card-box-->
							<div class="row m-t-50">
                                <div class="col-sm-12 text-center">
									<font color="#ffffff"> Kembali ke </font> <a href="{{ url('/') }}" class="text-primary m-l-5"><b> <font color="#87CEFA"> Halaman Utama</font></b></a></p>
								</div>
                            </div>

                        </div>
                        <!-- end wrapper -->
                    </div>
                </div>
            </div>
          </section>

		  <script>
            var resizefunc = [];
        </script>
</body>
@endsection
</html>
