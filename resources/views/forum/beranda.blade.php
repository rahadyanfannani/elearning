@extends('layouts.app_materi')
@section('navigation')
  <li class="home">
    <a href="{{ url('mahasiswa/dashboard')  }}">Dashboard </a>

  </li>
  <li>    <a href="{{ url('/mahasiswa/list_pertemuan/'.$kelas->id_kelas_virtual)  }}">Materi</a></li>

  @endsection
@section('content')
  <!-- Page title -->
  <div class="page-title parallax parallax1">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title-heading">
            <h1 class="title">Forum diskusi kelas {{ $kelas->matakuliah->nama_matkul." - ".$kelas->kelas->nama_kelas }} </h1>
            <div class="breadcrumbs">
              <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li>Forum</li>
              </ul>
            </div>
          </div><!-- /.page-title-captions -->

        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /.page-title -->
<div id="app" class="container">


 

<forum id-kelas="{{ $kelas->id_kelas_virtual }}" create-message-url="{{ url('/kelas/'.$kelas->id_kelas_virtual.'/create-message') }}" get-url="{{ url('/kelas/'.$kelas->id_kelas_virtual.'/get-message') }}"></forum>

</div>
<style>
  #btn-post{
    margin-top:10px
  }
  body {
  background: url('../../assets/images/BG Isi.png') no-repeat top center;
  margin: 0;
  background-size: cover;
  }
  #post-container {
    margin-top:20px;
  }
  .post-panel {
    margin-top:10px
  }
  #forum {
    margin-top:30px;
  }
</style>


@endsection
