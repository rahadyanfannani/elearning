<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
  <a href="{{ url('admin/dosen/tambah') }}">Tambah Dosen</a>
    <table>
      <tr>
        <th>Id dosen</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>No Telepon</th>
      </tr>
      @foreach ($dosen as $data)
      <tr>
        <td>{{ $data->id_dosen }}</td>
        <td>{{ $data->nama }}</td>
        <td>{{ $data->email }}</td>
        <td>{{ $data->alamat }}</td>
        <td>{{ $data->no_telp }}</td>
        <td><a href="{{ url('admin/dosen/edit/'.$data->id_dosen) }}">EDIT</a></td>
        <td><a href="{{ url('admin/dosen/hapus/'.$data->id_dosen) }}">HAPUS</a></td>
      </tr>
      @endforeach
    </table>
  </body>
</html>
