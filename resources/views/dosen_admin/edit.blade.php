<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Dosen</title>
  </head>
  <body>
    <form  action="{{ url('admin/dosen/edit/'.$dosen->id_dosen) }}" method="post">
      {{ csrf_field() }}
      <table>
        <tr>
          <td>Nama Dosen</td>
          <td>:</td>
          <td><input type="text" name="nama" value="{{$dosen->nama}}"></td>
        </tr>
        <tr>
          <td>Email</td>
          <td>:</td>
          <td><input type="email" name="email" value="{{$dosen->email}}"></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td>:</td>
          <td><input type="text" name="alamat" value="{{$dosen->alamat}}"></td>
        </tr>
        <tr>
          <td>No Telepon</td>
          <td>:</td>
          <td><input type="text" name="no_telp" value="{{$dosen->no_telp}}"></td>
        </tr>
      </table>
      <input type="SUBMIT"  value="Simpan">


    </form>

  </body>
</html>
