<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tambah Dosen</title>
  </head>
  <body>
    <form  action="{{ url('admin/dosen/tambah') }}" method="post">
      {{ csrf_field() }}
      <table>
        <tr>
          <td>Id Dosen </td>
          <td>:</td>
          <td><input type="text" name="id_dosen" ></td>
        </tr>
        <tr>
          <td>Nama Dosen</td>
          <td>:</td>
          <td><input type="text" name="nama"></td>
        </tr>
        <tr>
          <td>Email</td>
          <td>:</td>
          <td><input type="email" name="email"></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td>:</td>
          <td><input type="text" name="alamat"></td>
        </tr>
        <tr>
          <td>No Telepon</td>
          <td>:</td>
          <td><input type="text" name="no_telp"></td>
        </tr>
        <tr>
          <td>Password</td>
          <td>:</td>
          <td><input type="password" name=""></td>
        </tr>
      </table>
      <input type="SUBMIT"  value="Simpan">


    </form>

  </body>
</html>
