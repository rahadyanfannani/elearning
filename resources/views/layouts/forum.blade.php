<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ELearning - Teknik Elektro UM</title>
    <script>
        window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
    </script>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <script src="{{ asset('templateEditor/ckeditor/ckeditor.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="bs-docs-nav navbar navbar-static-top" id="top">
    <div class="container">
        <div class="navbar-header">
            <button aria-controls="bs-navbar" aria-expanded="false" class="collapsed navbar-toggle" data-target="#bs-navbar" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('mahasiswa/dashboard')  }}" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-chevron-left"></span>Kembali</a>

            <a class="navbar-brand" style="float:right;margin-left:5px">
                 {{ $kelas->matakuliah->nama_matkul." - ".$kelas->kelas->nama_kelas }}</a>
        </div>
        <nav class="collapse navbar-collapse" id="bs-navbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/mahasiswa/list_pertemuan/'.$kelas->id_kelas_virtual)  }}" >Materi</a></li>
                <li><a href="http://blog.getbootstrap.com" >{{ Auth::guard("mahasiswa")->user()->nama  }}</a></li>
            </ul>
        </nav>
    </div>
</header>
@yield('content')


<style>
    .bs-docs-nav {
        margin-bottom: 0;
        background-color: #fff;
        border-bottom: 0;
    }
</style>
<!--</div>-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/app.js')}}" type="text/javascript"></script>

</body>
</html>
