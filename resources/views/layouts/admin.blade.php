<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>E-Learning Elektro</title>

	<!-- BOOTSTRAP STYLES-->
    <link rel="stylesheet" href="{{ asset('assets/stylesheets/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('admin/css/style.css') }}" media="screen" title="no title">

     <!-- FONTAWESOME STYLES-->
    <link href="{{ asset('admin/css/font-awesome.css') }}" rel="stylesheet" />
        <!-- CUSTOM STYLES-->
    <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Admin<b>LTE</b></a>
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Last access : <?=(date("d M Y"))?> &nbsp;
<a href="#">
 <button type="button" name="button" class="btn btn-danger">Logout</button>
 </a>
</div>
        </nav>
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="{{ asset('admin/img/find_user.png') }}" class="user-image img-responsive"/>
					</li>


                    <li>
                        <a  href="{{ url('admin/home') }}"> Beranda</a>
                    </li>
                    <li>
                        <a  href="{{ url('admin/matakuliah') }}"> Matakuliah</a>
                    </li>
                    <li>
                        <a  href="{{ url('admin/dosen') }}"> Dosen</a>
                    </li>
                    <li>
                        <a  href="{{ url('admin/kelas') }}"> Kelas</a>
                    </li>
                    <li>
                        <a  href="{{ url('admin/kelas_virtual') }}"> Kelas Virtual</a>
                    </li>


                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     @yield('content')

                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />

    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
        </div>
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{ asset('admin/js/jquery-1.10.2.js') }}"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{ asset('admin/js/jquery.metisMenu.js') }}"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="{{ asset('admin/js/custom.js') }}"></script>


</body>
</html>
