<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Materi - ELearning - Teknik Elektro UM</title>

  <!-- App CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">

  <!-- JS -->
  <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>
</head>

<body class="body-dashboard-dos">
@extends('layouts.appv1')
@section('content')
<div class="row top-head">
  <div class="row" style="padding-left: 20px; padding-top: 30px;">
    <a href="{{ url('dosen/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>
</div>
</div>
<div class="row content-dashboard-dos">
<div class="row" style="padding-left: 20px; padding-top: 10px;">
    <a href="{{ url('dosen/dashboard') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke dashboard </span></button></a>
</div>
<div class="col-sm-12">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Daftar Soal</h3>
    </div>
    <div class="box-body">
      <?php
      if ($count!=0) {?>
        <table class="table table-bordered" id="table_soal">
          <tr>
            <th>#</th>
            <td>Soal</td>
            <td>Tipe Jawaban</td>
            <td>Optional</td>
          </tr>
          <?php $i=1;
          foreach ($data as $key => $value) {?>
            <tr>
              <td>{{ $i }}</td>
              <td>{!! $value->soal !!}</td>
              <td>{{ $value->tipe_jawaban }}</td>
              <td>
                <button type="button" name="button" class="btn btn-xs btn-success" onclick="EditSoal('{{ $i }}','{{ $value->id }}')">
                  EDIT
                </button>
                <a href="{{ url('dosen/hapusSoalEssay/'.$value->id) }}">
                <button type="button" name="button" class="btn btn-xs btn-danger">
                  HAPUS
                </button>
                </a>
              </td>
            </tr>
          <?php $i++;}
           ?>
        </table>
      <?php }else {?>
        <div class="alert alert-danger">
          Data Kosong!
        </div>
      <?php }
       ?>
    </div>
    <div class="box-footer">
      <button type="button" name="button" class="btn btn-primary" onclick="AddSoal()">
        Tambah Soal
      </button>
    </div>
  </div>

  <div class="modal modal-primary" id="modal_soal">
    <div class="modal-dialog" style="width:75%">
      <div class="modal-content" >
        <div class="modal-header">
          <h3 class="modal-title">Form Soal</h3>
        </div>
        <form class="form-horizontal" action="{{ url('dosen/saveEssay') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-2">Soal</label>
            <div class="col-sm-10">
              <input type="hidden" name="action" id="action" value="0">
              <input type="hidden" name="id" id="id" value="0">
              <input type="hidden" name="id_essay" value="{{ $id_essay }}">
              <textarea name="soal" class="form-control ckeditor" id="soal" rows="8" cols="80" required="required"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2">Tipe Jawaban</label>
            <div class="col-sm-10">
              <select class="form-control" name="tipe_jawaban" id="tipe_jawaban" required="required">
                <option value="text">Uraian Text</option>
                <<option value="file">File</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
          <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
  <script type="text/javascript">
       CKEDITOR.replace('soal');
    </script>
  <script type="text/javascript">
  var modal_soal=document.getElementById("modal_soal");
  var tabel=document.getElementById('table_soal');
  function AddSoal(){
    document.getElementById('action').value='0';
    document.getElementById('tipe_jawaban').selectedIndex='0';
    CKEDITOR.instances['soal'].setData("");
    modal_soal.style.display='block';
  }
  function EditSoal(row,id){
    document.getElementById('action').value='1';
    document.getElementById('id').value=id;
    var tipe=table_soal.rows[row].cells[2].innerHTML;
    if(tipe=="file") document.getElementById('tipe_jawaban').selectedIndex='1';
    else document.getElementById('tipe_jawaban').selectedIndex='0';
    CKEDITOR.instances['soal'].setData(table_soal.rows[row].cells[1].innerHTML);
    modal_soal.style.display='block';
  }

  function CloseModal(){
    modal_soal.style.display='none';

  }
  window.onclick=function(event){
    if(event.target==modal_soal)  {
      modal_soal.style.display='none';
    }
  }
  </script>
@endsection
</body>
</html>
