{{--<html lang="en">--}}
{{--<head>--}}
  {{--<meta charset="UTF-8">--}}
  {{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
  {{--<meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
  {{--<title>Tambah Evaluasi - ELearning - Teknik Elektro UM</title>--}}

  {{--<!-- App CSS -->--}}
  {{--<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">--}}
  {{--<link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">--}}
  {{--<link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">--}}
  {{--<!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->--}}
  {{--<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">--}}
  {{--<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">--}}
  {{--<link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">--}}

  {{--<!-- JS -->--}}
  {{--<script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>--}}
{{--</head>--}}

{{--<body class="body-dashboard-dos">--}}
@extends('layouts.appv1')
@section('content')
<div class="row top-head">
<div class="col-sm-10"> </div>
<div class="col-sm-1" style="margin-left: 100px;">
  <div class="row" style="padding-left: 20px; padding-top: 30px;">
    <a href="{{ url('dosen/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>
</div>
</div>

<div class="row content-dashboard-dos">
<div class="row" style="padding-left: 20px; padding-top: 30px;">
    <a href="{{ url('dosen/dashboard') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke dashboard </span></button></a>
</div>
</div>
<div class="row" style="margin-top: 15px;">
 <div class="col-sm-1"> </div>
  <div class="col-sm-10">
    <div id="app">
    <evaluasi-editor post-url="{{ url("/dosen/tambah-evaluasi/".$pertemuan->id_pertemuan)  }}" redirect-url="{{ url("/dosen/materi/".$pertemuan->id_pertemuan)  }}"></evaluasi-editor>
    </div>
  </div>
 <div class="col-sm-1"> </div>
 </div>
</div>

@endsection
</body>
</html>