
@extends('layouts.appdosen')
@section('content')

  <!-- Page title -->
  <div class="page-title parallax parallax1">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title-heading">
            <h1 class="title">Selamat Datang di Halaman Dashboard Dosen</h1>
            <div class="breadcrumbs">
              <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li>Data Kelas</li>
              </ul>
            </div>
          </div><!-- /.page-title-captions -->

        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /.page-title -->

  <!-- Fillter courses -->
  <section class="flat-row pad-top50px pad-bottom0px">
    <div class="container">
      <div class="row">
        <div class="search-course">
          <div class="col-md-10">
        <table class="table m-0 table-colored-full table-primary">
        <thead>
          <tr>
            <th>Matakuliah</th>
            <th>Kelas</th>
            <th>Opsi</th>
          </tr>
          </thead>
          <?php foreach ($kelasVirtual as $value) {?>

            <tr>
              <td>{{ $value->matakuliah->nama_matkul }}</td>
              <td>{{ $value->kelas->nama_kelas }}</td>
              <td>
                <a href="{{ url('dosen/kelas/'.$value->id_kelas_virtual) }}"><button type="button" name="button" class="btn btn-info btn-rounded w-md waves-light">Daftar Anggota</button></a>
                <a href="{{ url('dosen/pertemuan/'.$value->id_kelas_virtual) }}"><button type="button" name="button" class="btn btn-custom btn-rounded w-md waves-light">Daftar Pertemuan</button></a>
              </td>
            </tr>

          <?php } ?>
        </table>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
