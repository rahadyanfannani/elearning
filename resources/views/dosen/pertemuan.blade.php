
@extends('layouts.appdosen')
@section('content')
  <!-- Page title -->
  <div class="page-title parallax parallax1">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="page-title-heading">
            <h1 class="title">Data Pertemuan</h1>
            <div class="breadcrumbs">
              <ul>
                <li><a href="index.html">Dashboard</a></li>
                <li>Data Pertemuan</li>
              </ul>
            </div>
          </div><!-- /.page-title-captions -->

        </div><!-- /.col-md-12 -->
      </div><!-- /.row -->
    </div><!-- /.container -->
  </div><!-- /.page-title -->

    <a href="{{ url('dosen/dashboard') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke dashboard </span></button></a>


        <table class="table m-0 table-colored-full table-hover" id="table_pertemuan">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Opsi</th>
          </tr>
          </thead>
          <?php $i=1; foreach ($pertemuan as $value) {?>
            <tr>
              <td>{{ $i }}</td>
              <td>{{ $value->nama }}</td>
              <td>
                <a href="{{ url('dosen/materi/'.$value->id_pertemuan) }}">
                  <button type="button" name="button" class="btn btn-info btn-rounded w-md waves-light">
                    <i class="fa fa-file m-r-5"> </i> <span>
                    Detail </span>
                  </button>
                </a>

                <button type="button" name="button" onclick="EditPertemuan(<?php echo $i;?>,'<?php echo $value->id_pertemuan;?>')" class="btn btn-rounded w-md waves-light btn-custom">
                <i class="fa fa-pencil m-r-5"> </i> <span>
                Edit </span>
              </button>

              <a href="{{ url('dosen/hapusPertemuan/'.$value->id_pertemuan.'/'.$value->id_kelas_virtual) }}">
                <button type="button" name="button" onclick="Hapus('<?php echo $i;?>')" class="btn btn-rounded w-md waves-light btn-danger">
                  <i class="fa fa-trash-o m-r-5"> </i> <span>
                  Hapus </span>
                </button>
              </a>
                  <button onclick="location.href='{{url("dosen/pertemuan/sort/".$value->id_pertemuan."/up")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-up m-r-5"> </i> </button><button onclick="location.href='{{url("dosen/pertemuan/sort/".$value->id_pertemuan."/down")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-down m-r-5"> </i> </button>
              </td>
            </tr>
          <?php $i++;  } ?>
        </table>
        </div>
      </div>
      <div class="box-footer">
        <button type="button" name="button" class="btn btn-primary btn-sm" onclick="AddPertemuan()"> <i class="fa fa-plus m-r-5"> </i> <span> Tambah Pertemuan </span></button>
      </div>
    </div>

    <div class="modal modal-primary" id="modal_pertemuan">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
             <h4 class="modal-title">Pertemuan</h4>
          </div>
          <form class="form-horizontal" action="{{ url('dosen/tambahPertemuan') }}" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <div class="form-group">
              <label class="control-label col-sm-3">Nama Pertemuan</label>
              <div class="col-sm-9">
                <input type="hidden" name="action" value="0" id="action">
                <input type="hidden" name="id_pertemuan" value="0" id="id_pertemuan">
                <input type="hidden" name="id_kelas_virtual" value="{{ $id }}">
                <input type="text" name="nama" placeholder="Nama Pertemuan" id="nama" class="form-control" required="required">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="submit" name="save" value="Simpan" class="btn btn-outline pull-right">
            <button type="button" class="btn btn-outline pull-left" onclick="CloseModal()" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>

    <script type="text/javascript">
    var modal1=document.getElementById("modal_pertemuan");
    var tabel=document.getElementById("table_pertemuan");
    function AddPertemuan(){
      document.getElementById("action").value='0';
      document.getElementById("id_pertemuan").value='0';
      document.getElementById("nama").value='';
      modal1.style.display="block";
    }
    function EditPertemuan(row,id){
      document.getElementById("action").value='1';
      document.getElementById("id_pertemuan").value=id;
      document.getElementById("nama").value=tabel.rows[row].cells[1].innerHTML;
      modal1.style.display="block";
    }
    function CloseModal(){
      modal1.style.display='none';
    }
    window.onclick=function(event){
      if(event.target==modal1)  {
        modal1.style.display='none';
      }
    }
    </script>

@endsection
