<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Materi - ELearning - Teknik Elektro UM</title>

  <!-- App CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">

  <!-- JS -->
  <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>
</head>

<body class="body-dashboard-dos">
@extends('layouts.appv1')
@section('content')
<div class="row top-head">
<div class="col-sm-10"> </div>
<div class="col-sm-1" style="margin-left: 100px;">
  <div class="row" style="padding-top: 30px;">
    <a href="{{ url('dosen/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>
</div>
</div>
</div>
<div class="row content-dashboard-dos">
<div class="row" style="padding-left: 20px; padding-top: 10px;">
    <a href="{{ url('dosen/dashboard') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke dashboard </span></button></a>
</div>
<div class="col-sm-12">
<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $pertemuan->nama." (".$pertemuan->kelasVirtual->matakuliah->nama_matkul.")" }}</h3>
  </div>
  <div class="col-sm-12">
  <div class="card-box-trans">
  <div class="box-body">
  <!-- Tabs left -->
  <div class="tabs-vertical-env">
    <ul class="nav tabs-vertical">
      <li class="active"><a href="#tambah" data-toggle="pill" >Tambah Materi</a></li>
      <?php  foreach ($detail as $key => $value) {?>
       @if($value->tipe == "evaluasi")
        <li><a href="#evaluasi{{ $value->id }}" data-toggle="pill" class="text_wrap">      <i class="fa fa-bars m-r-5"> </i> <span class="pull-left"> {{ $value->konten->nama }} </span><button onclick="location.href='{{url("dosen/sort/".$value->id."/up")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-up m-r-5"> </i></button><button onclick="location.href='{{url("dosen/sort/".$value->id."/down")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-down m-r-5"> </i></button></a></li>
      @elseif($value->tipe == "essay")
        <li><a href="#essay{{ $value->id }}" data-toggle="pill" class="text_wrap"> <i class="fa fa-bars m-r-5"> </i> <span class="pull-left"> {{ $value->konten->nama }} </span><button onclick="location.href='{{url("dosen/sort/".$value->id."/up")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-up m-r-5"> </i></button><button onclick="location.href='{{url("dosen/sort/".$value->id."/down")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-down m-r-5"> </i></button> </a></li>
      @else
        <li><a href="#{{ $value->id }}" data-toggle="pill" class="text_wrap"> <i class="fa fa-bars m-r-5"> </i> <span class="pull-left"> {{ $value->konten->nama }} </span><button onclick="location.href='{{url("dosen/sort/".$value->id."/up")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-up m-r-5"> </i></button><button onclick="location.href='{{url("dosen/sort/".$value->id."/down")}}'" class="btn waves-light btn-custom"><i class="fa fa-arrow-down m-r-5"> </i> </button></a></li>
      @endif
      <?php }?>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active" id="tambah">
        <div class="box">
         <a class="btn btn-info btn-rounded w-md waves-light" href="{{ url("/dosen/tambahMateri/".$pertemuan->id_pertemuan)  }}"> <i class="fa fa-book m-r-5"> </i> <span> Materi </span></a>

         <a class="btn btn-custom btn-rounded w-md waves-light" href="{{ url("/dosen/tambah-evaluasi/".$pertemuan->id_pertemuan)  }}"> <i class="fa fa-edit m-r-5"> </i> <span>Soal Pilihan Ganda</span></a>
         <button type="button" name="button" class="btn btn-custom btn-rounded w-md waves-light" onclick="AddEssay()"><i class="fa fa-edit m-r-5" > </i> <span>Soal Essay</span></button>


        </div>
      </div>

            <?php foreach ($pertemuan->detail as $key => $value) {?>
              <?php if($value->tipe == "materi") { ?>
              <div class="tab-pane" id="{{ $value->id }}">
                   <h4>{{ $value->konten->nama }}</h4>
                   <a href="{{ url('dosen/hapusMateri/'.$value->id) }}">
                     <button type="button" name="button" class="btn btn-danger pull-right"> <i class="fa fa-trash-o m-r-5"> </i> <span> Hapus Materi </span></button>
                   </a>

                     <button type="button" name="button" class="btn btn-info" onclick="EditNamaMateri('{{ $value->konten->id }}','{{ $value->konten->nama }}')"> <i class="fa fa-pencil m-r-5"> </i> <span> Edit Nama Materi </span></button>
                   <br>
                   <?php if (file_exists(public_path('upload/content_utama/'.$value->konten->konten_utama)) && $value->konten->konten_utama != "") {?>
                     <video width="320" height="240" controls>
                       <source src="{{ asset('upload/content_utama/'.$value->konten->konten_utama) }}" type="video/mp4">
                         Your browser does not support the video tag.
                    </video>
                    <br>
                    <button type="button" name="button" class="btn btn-info" onclick="EditKontenUtama('{{ $value->konten->id }}','{{ $value->konten->konten_utama }}')"> <i class="fa fa-pencil m-r-5"> </i> <span> Ubah File </span></button>
                    <button type="button" name="button" class="btn btn-danger" onclick="location.href = '{{ url("dosen/hapus-file/".$value->id) }}'">  Hapus File</button>
                   <?php } else { ?>
                <button type="button" name="button" class="btn btn-info" onclick="TambahKontenUtama('{{ $value->konten->id }}')"> <i class="fa fa-pencil m-r-5"> </i> <span> Tambah Konten Utama </span></button>

              <?php  } ?>

                  <p>
                    {!! $value->konten->deskripsi !!}
                    <br>
                    <button type="button" name="button" class="btn btn-info" onclick="EditDeskripsi('{{ $value->id }}')"> <i class="fa fa-pencil m-r-5"> </i> <span> Edit Deskripsi </span> </button>
                  </p>
                  <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">File Lampiran</h3>
                    </div>
                    <div class="col-sm-12">
                    <div class="box-body">
                      <table class="table m-0 table-colored-full table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama Dokumen</th>
                          <th>Opsi</th>
                        </tr>
                        </thead>
                        <?php
                        $i=1;
                        foreach ($value->konten->dokumen as $ky => $val) {?>
                          <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $val->nama }}</td>
                            <td>
                              <a href="{{ asset('upload/dokumen/'.$val->nama) }}">
                              <button type="button" name="button" class="btn btn-success"> <i class="fa fa-download m-r-5"> </i> <span>
                                Download File </span>
                              </button>
                              </a>
                              <a href="{{ url('dosen/hapusDokumen/'.$val->id_dokumen.'/'.$pertemuan->id_pertemuan) }}">
                              <button type="button" name="button" class="btn btn-danger"> <i class="fa fa-trash-o m-r-5"> </i> <span>
                                Hapus File </span>
                              </button>
                              </a>
                            </td>
                          </tr>
                        <?php $i++; }
                         ?>
                      </table>
                    </div>
                    <div class="box-footer">
                      <button type="button" name="button" class="btn btn-info pull-right" onclick="AddDokumen('{{ $value->konten->id }}')"> <i class="fa fa-plus-circle m-r-5"> </i> <span>
                        Tambah Dokumen </span>
                      </button>
                    </div>
                  </div>
                  </div>
              </div>

            <?php
          }elseif ($value->tipe == "essay") {?>
            <div class="tab-pane" id="essay{{ $value->id }}">
              <h4>{{ $value->konten->nama }}</h4>
              <table class="table table-bordered" id="table_soal">
                <tr>
                  <th>#</th>
                  <td>Soal</td>
                  <td>Tipe Jawaban</td>
                  <td>Optional</td>
                </tr>
                <?php $i=1;
                foreach ($value->konten->soal as $ky=>$val) {?>
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{!! $val->soal !!}</td>
                    <td>{{ $val->tipe_jawaban }}</td>
                    <td>
                      <button type="button" name="button" class="btn btn-xs btn-success" onclick="EditSoal('{{ $i }}','{{ $val->id }}','{{ $val->id_essay }}')">
                        EDIT
                      </button>
                      <a href="{{ url('dosen/hapusSoalEssay/'.$val->id) }}">
                      <button type="button" name="button" class="btn btn-xs btn-danger">
                        HAPUS
                      </button>
                      </a>
                    </td>
                  </tr>
                <?php $i++;}
                 ?>
              </table>
              <button type="button" name="button" class="btn btn-primary" onclick="AddSoal('{{ $value->id_konten }}')">
                Tambah Soal
              </button>
            </div>
          <?php

           ?>
        <?php }else if($value->tipe == "evaluasi"){ ?>

      <div class="tab-pane" id="{{ "evaluasi".$value->id }}">
        <a href="{{ url('/dosen/delete-evaluasi/'.$value->id)  }}" class="btn btn-danger">Hapus Evaluasi</a>
        <div id="app">
          <evaluasi-editor edit="true" get-url="{{ url("dosen/evaluasi/".$value->id )}}" redirect-url="{{  url("/dosen/materi/".$value->id)  }}" post-url="{{ url("/dosen/edit-evaluasi/".$value->id)  }}"></evaluasi-editor>
        </div>
      </div>

        <?php  }} ?>
    </div>
    </div>
  </div>
  </div>
  </div>
</div>
</div>
</div>

<!-- modal judul materi -->
<div class="modal modal-danger" id="modal_nama">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nama Materi</h4>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/update_nama_materi') }}" method="post">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-3">Nama Materi</label>
            <div class="col-sm-9">
              <input type="hidden" name="edit_id_materi_nama" id="edit_id_materi_nama">
              <input type="hidden" name="edit_id_pertemuan_nama" id="edit_id_pertemuan_nama" value="{{ $pertemuan->id_pertemuan }}">
              <input type="text" name="edit_nama_materi" required="required" class="form-control" id="edit_nama_materi">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-outline pull-right">
          <button type="button" class="btn btn-outline pull-left" onclick="CloseModal()" data-dismiss="modal"> Close </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- end modal judul materi -->

<!-- modal konten utama -->
<div class="modal modal-danger" id="modal_konten">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konten Utama</h4>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/update_konten_utama') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-3">Upload File</label>
            <div class="col-sm-9">
              <input type="hidden" name="edit_konten_utama_lawas" id="edit_konten_utama_lawas" value="">
              <input type="hidden" name="edit_id_materi_konten_utama" id="edit_id_materi_konten_utama">
              <input type="hidden" name="edit_id_pertemuan_konten_utama" id="edit_id_pertemuan_konten_utama" value="{{ $pertemuan->id_pertemuan }}">
              <input type="file" name="edit_konten_utama" required="required" class="form-control" id="edit_konten_utama">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-outline pull-right">
          <button type="button" class="btn btn-outline pull-left" onclick="CloseModal()" data-dismiss="modal"> Close </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- end modal konten utama -->




<!-- modal konten utama -->
<div class="modal modal-danger" id="modal_tambah_konten">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konten Utama</h4>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/tambah_konten_utama') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-3">Upload File</label>
            <div class="col-sm-9">
              <input type="hidden" name="edit_konten_utama_lawas" id="edit_konten_utama_lawas" value="">
              <input type="hidden" name="tambah_id_materi_konten_utama" id="tambah_id_materi_konten_utama">
              <input type="hidden" name="edit_id_pertemuan_konten_utama" id="edit_id_pertemuan_konten_utama" value="{{ $pertemuan->id_pertemuan }}">
              <input type="file" name="edit_konten_utama" required="required" class="form-control" id="edit_konten_utama">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-outline pull-right">
          <button type="button" class="btn btn-outline pull-left" onclick="CloseModal()" data-dismiss="modal"> Close </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- end modal konten utama -->




<!-- modal dokumen -->
<div class="modal modal-danger" id="modal_dokumen">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Dokumen Lampiran</h4>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/tambahDokumen') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-sm-4">Dokumen Lampiran</label>
            <div class="col-sm-8">
              <input type="hidden" name="dokumen_id_materi" id="dokumen_id_materi">
              <input type="hidden" name="dokumen_id_pertemuan" id="dokumen_id_pertemuan" value="{{ $pertemuan->id_pertemuan }}">
              <input type="file" name="dokumen_nama" id="dokumen_nama" required="required">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="save" value="Simpan" class="btn btn-outline pull-right">
          <button type="button" class="btn btn-outline pull-left" onclick="CloseModal()" data-dismiss="modal"> Close </button>
        </div>
      </form>

    </div>
  </div>
</div>
<!-- end modal dokumen -->

<div class="modal modal-primary" id="modal_essay">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Judul Soal</h3>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/simpanEssay') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2">Judul Test</label>
          <div class="col-sm-10">
            <input type="hidden" name="id_pertemuan" value="{{ $pertemuan->id_pertemuan }}">
            <input type="text" name="nama" placeholder="Contoh: UTS/Ulangan Harian/UAS" class="form-control" required="required">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
        <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
      </div>
    </form>
    </div>
  </div>
</div>

<div class="modal modal-primary" id="modal_soal">
  <div class="modal-dialog" style="width:75%">
    <div class="modal-content" >
      <div class="modal-header">
        <h3 class="modal-title">Form Soal</h3>
      </div>
      <form class="form-horizontal" action="{{ url('dosen/saveEssay') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-sm-2">Soal</label>
          <div class="col-sm-10">
            <input type="hidden" name="action" id="action" value="0">
            <input type="hidden" name="id" id="id" value="0">
            <input type="hidden" name="id_essay" value="" id="id_essay">
            <textarea name="soal" class="form-control ckeditor" id="soal" rows="8" cols="80" required="required"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2">Tipe Jawaban</label>
          <div class="col-sm-10">
            <select class="form-control" name="tipe_jawaban" id="tipe_jawaban" required="required">
              <option value="text">Uraian Text</option>
              <<option value="file">File</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
        <button type="button" class="btn btn-outline btn-danger pull-left" onclick="CloseModal()" data-dismiss="modal"> <i class="fa fa-times-circle m-r-5"> </i> <span> Close </span></button>
      </div>
    </form>
    </div>
  </div>
</div>

<script type="text/javascript">


     CKEDITOR.replace( 'edit_deskripsi', {
         extraPlugins: 'uploadimage',
         height: 300,

         // Upload images to a CKFinder connector (note that the response type is set to JSON).
         uploadUrl: '{{ url("/dosen/upload-image?_token=".csrf_token())  }}',

         // The following options are not necessary and are used here for presentation purposes only.
         // They configure the Styles drop-down list and widgets to use classes.


     } );
  </script>
<script type="text/javascript">
var modal_nama=document.getElementById("modal_nama");
var modal_konten=document.getElementById("modal_konten");
var modal_tambah_konten = document.getElementById("modal_tambah_konten");
var modal_deskripsi=document.getElementById("modal_deskripsi");
var modal_dokumen=document.getElementById("modal_dokumen");
var modal_essay=document.getElementById("modal_essay");
var modal_soal=document.getElementById("modal_soal");
var tabel=document.getElementById('table_soal');
function AddSoal(id){
  document.getElementById('action').value='0';
  document.getElementById('tipe_jawaban').selectedIndex='0';
  document.getElementById('id_essay').value=id;
  CKEDITOR.instances['soal'].setData("");
  modal_soal.style.display='block';
}
function EditSoal(row,id,ide){
  document.getElementById('action').value='1';
  document.getElementById('id').value=id;
  document.getElementById('id_essay').value=ide;
  var tipe=table_soal.rows[row].cells[2].innerHTML;
  if(tipe=="file") document.getElementById('tipe_jawaban').selectedIndex='1';
  else document.getElementById('tipe_jawaban').selectedIndex='0';
  CKEDITOR.instances['soal'].setData(table_soal.rows[row].cells[1].innerHTML);
  modal_soal.style.display='block';
}
function AddEssay(){
  modal_essay.style.display="block";
}
function AddDokumen(id){
  document.getElementById("dokumen_id_materi").value=id;
  modal_dokumen.style.display="block";
}
function EditNamaMateri(id,nama){
  document.getElementById("edit_id_materi_nama").value=id;
  document.getElementById("edit_nama_materi").value=nama;
  modal_nama.style.display="block";
}
function TambahKontenUtama(id){

  document.getElementById("tambah_id_materi_konten_utama").value=id;
  modal_tambah_konten.style.display="block";
}
function EditDeskripsi(id,deskripsi){
 // document.getElementById("edit_id_materi_deskripsi").value=id;
  //document.getElementById("edit_deskripsi").value=deskripsi;
 // CKEDITOR.instances['edit_deskripsi'].setData(deskripsi);
  //modal_deskripsi.style.display="block";
  location.href="{{ url('dosen/edit-deskripsi')  }}/"+id;
}
function CloseModal(){
  modal_nama.style.display='none';
  modal_konten.style.display='none';
  modal_deskripsi.style.display='none';
  modal_dokumen.style.display='none';
  modal_essay.style.display='none';
  modal_soal.style.display='none';
}
window.onclick=function(event){
  if(event.target==modal_nama || event.target==modal_konten || event.target==modal_deskripsi || event.target==modal_dokumen || event.target==modal_essay || event.target==modal_soal)  {
    modal_nama.style.display='none';
    modal_konten.style.display='none';
    modal_deskripsi.style.display='none';
    modal_dokumen.style.display='none';
    modal_essay.style.display='none';
    modal_soal.style.display='none';
  }
}
</script>
@endsection
</body>
</html>