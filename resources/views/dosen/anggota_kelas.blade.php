<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Anggota Kelas - ELearning - Teknik Elektro UM</title>

  <!-- App CSS -->
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">

  <!-- JS -->
  <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>
</head>

<body class="body-dashboard-dos">
@extends('layouts.appv1')
@section('content')
<div class="row top-head">
<div class="col-sm-10"> </div>
<div class="col-sm-1" style="margin-left: 100px;">
  <div class="row" style="padding-top: 30px;">
    <a href="{{ url('dosen/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>
</div>
</div>
</div>
<div class="row content-dashboard-dos">
<div class="row" style="padding-left: 20px; padding-top: 10px;">
   	<a href="{{ url('dosen/dashboard') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke dashboard </span></button></a>
</div>
<div class="col-sm-11">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Data Anggota Kelas</h3>
      </div>
      <div class="box-content">
      <div class="table-responsive" data-pattern="priority-columns">
        <table class="table m-0 table-colored-full table-hover">
        <thead> 
          <tr>
            <th>NIM</th>
            <th>Nama</th>
            <th>Status</th>
          </tr>
          </thead>
          <?php foreach ($anggota as $value) {?>

            <tr>
              <td>{{ $value->mahasiswa->nim }}</td>
              <td>{{ $value->mahasiswa->nama }}</td>
              <td>
                <?php if ($value->status=='0') {?>
                  <a href="{{ url('dosen/accept/'.$value->id_kelas_virtual.'/'.$value->mahasiswa->nim.'/1') }}"><button type="button" name="button" class="btn btn-sm btn-success"> <i class="fa fa-check m-r-5"> </i> <span> Terima </span></button></a>
                  <a href="{{ url('dosen/accept/'.$value->id_kelas_virtual.'/'.$value->mahasiswa->nim.'/2') }}"><button type="button" name="button" class="btn btn-sm btn-danger"> <i class="fa fa-times m-r-5"> </i> <span> Tolak</span></button></a>
                <?php }else if ($value->status=='1') {?>
                  <span class="label label-success">
                    Disetujui
                  </span>
                <?php }else if ($value->status=='2') {?>
                  <span class="label label-danger">
                    Ditolak
                  </span>
                <?php } ?>

              </td>
            </tr>
          <?php } ?>
        </table>
      </div>
      </div>
    </div>
  </div>
  </div>
  </body>
</html>
@endsection
