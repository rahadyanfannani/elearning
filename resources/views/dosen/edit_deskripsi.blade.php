<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Deskripsi - ELearning - Teknik Elektro UM</title>

    <!-- App CSS -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/pages.css') }}" rel="stylesheet">
<!-- <link href="{{ asset('assets/css/menu.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/core.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/components.css') }}" rel="stylesheet">

    <!-- JS -->
    <script src="{{ asset('js/modernizr.min.js') }}" type="text/javascript"></script>
</head>

<body class="body-dashboard-dos">
@extends('layouts.appv1')
@section('content')
    <div class="row top-head">
        <div class="col-sm-10"> </div>
        <div class="col-sm-1" style="margin-left: 100px;">
            <div class="row" style="padding-left: 20px; padding-top: 30px;">
                <a href="{{ url('dosen/logout') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <span> Logout </span></button></a>
            </div>
        </div>
    </div>
    <div class="row content-dashboard-dos">
        <div class="row" style="padding-left: 20px; padding-top: 10px;">
            <a href="{{ url('dosen/dashboard/') }}"><button type="button" name="button" class="btn btn-danger btn-rounded w-md waves-light"> <i class="fa fa-arrow-left m-r-5"> </i> <span> Kembali ke Dashboard </span></button></a>
        </div>
        <div class="row" style="margin-top: 15px;">
            <div class="col-sm-1"> </div>
            <div class="col-sm-10">
                <div class="card-box">
                    <h3 style="margin-top: 10px; margin-bottom: 10px;"> Edit Deskripsi </h3>
                    <form class="form-horizontal" action="{{ url('dosen/edit-deskripsi/'.$detail->id) }}" method="post" >
                        {{ csrf_field() }}
                        <div class="box-body">
                            <input type="hidden" name="id_materi" value="0">
                            <input type="hidden" name="id_pertemuan" value="{{ $detail->id_pertemuan }}">

                            <div class="form-group">
                                <label class="control-label col-sm-2">Deskripsi</label>
                                <div class="col-sm-8">
                                    <textarea name="deskripsi" id="deskripsi"  rows="8" cols="80" >
                                        {!! $detail->konten->deskripsi !!}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <input type="submit" name="save" value="Simpan" class="btn btn-primary pull-right">
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-1"> </div>
        </div>
    </div>
    <script>

        CKEDITOR.replace( 'deskripsi', {
            extraPlugins: 'uploadimage',
            height: 300,

            // Upload images to a CKFinder connector (note that the response type is set to JSON).
            uploadUrl: '{{ url("/dosen/upload-image?_token=".csrf_token())  }}',

            // The following options are not necessary and are used here for presentation purposes only.
            // They configure the Styles drop-down list and widgets to use classes.


        } );
    </script>
@endsection
</body>
</html>