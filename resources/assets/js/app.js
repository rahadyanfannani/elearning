
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 Vue.config.debug = true;

 Vue.component('forum',require('./components/Forum.vue'));
Vue.component('example', require('./components/Example.vue'));
Vue.component('evaluasi',require('./components/Evaluasi.vue'));
Vue.component('evaluasi-editor',require('./components/EvaluasiEditor.vue'));
const app = new Vue({
    el: '#app'
});
