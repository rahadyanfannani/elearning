var ajaxku;
var base_url="http://127.0.0.1/bill/";
function buatajax(){
	if (window.XMLHttpRequest){
		return new XMLHttpRequest();
	}
	if (window.ActiveXObject){
		return new ActiveXObject("Microsoft.XMLHTTP");
	}
	return null;
}
function TampilDetailNota(no_nota,div){
	ajaxku = buatajax();
	ajaxku.onreadystatechange=function(){InHTML(div)};
	ajaxku.open("POST",base_url+"dashboard/ambil_detail_nota/",true);
	ajaxku.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajaxku.send("no_nota="+no_nota);
}
function CariHutang(nama,div){
	ajaxku = buatajax();
	ajaxku.onreadystatechange=function(){InHTML(div)};
	ajaxku.open("POST",base_url+"dashboard/CariHutang/",true);
	ajaxku.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajaxku.send("nama="+nama);
}
function CariPiutang(nama,div){
	ajaxku = buatajax();
	ajaxku.onreadystatechange=function(){InHTML(div)};
	ajaxku.open("POST",base_url+"dashboard/CariPiutang/",true);
	ajaxku.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajaxku.send("nama="+nama);
}
function Report(bulan,tahun,filter,div){
	ajaxku = buatajax();
	ajaxku.onreadystatechange=function(){InHTML(div)};
	ajaxku.open("POST",base_url+"dashboard/laporan_kirim/",true);
	ajaxku.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	ajaxku.send("bulan="+bulan+"&tahun="+tahun+"&filter="+filter);
}

function Cetak(divid){
	var headstr = "<html><head><title></title></head><body>";
	var footstr = "</body>";
	var newstr = document.all.item(divid).innerHTML;
	var oldstr = document.body.innerHTML;
	document.body.innerHTML = headstr+newstr+footstr;
	window.print();
	document.body.innerHTML = oldstr;
	return false;
}
function InHTML(id){
	var data;
	if (ajaxku.readyState==4){
		data=ajaxku.responseText;
		if(data.length>0){
			document.getElementById(id).innerHTML = data;
		}else{
			document.getElementById(id).innerHTML = "Data Tidak Ada!";
		}
	}
}
